/* $Id$ */

/*
 * hed - Hexadecimal editor
 * Copyright (C) 2004  Petr Baudis <pasky@ucw.cz>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of version 2 of the GNU General Public License as
 * published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * At that moment, among the trees nearby, a horn rang out. It rent the night
 * like fire on a hill-top.
 *
 *      AWAKE! FEAR! FIRE! FOES! AWAKE!
 */

#include <config.h>

#include <ctype.h>
#include <curses.h>
#include <stdlib.h>
#include <string.h>

#include <hed.h>

#include <term/term.h>


char *errmsg_queued = NULL;

void
errmsg(char *err)
{
	errmsg_queued = err;
}

void
errmsg_show(char *err)
{
	int errmsg_y = term_get_max_y() - 1;
	term_clear_line(errmsg_y);
	term_print_string(0, errmsg_y, COLOR_ERROR, err);
}
