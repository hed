/* $Id$ */

/*
 * hed - Hexadecimal editor
 * Copyright (C) 2004  Petr Baudis <pasky@ucw.cz>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of version 2 of the GNU General Public License as
 * published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * Many times he repeated these words in different order, or varied them.
 * Then he tried other spells. one after another, speaking now faster and
 * louder, now soft and slow. Then he spoke many single words of Elvish speech.
 * Nothing happened. The cliff towered into the night, the countless stars were
 * kindled, the wind blew cold, and the doors stood fast.
 */

#include <config.h>

#include <ctype.h>
#include <curses.h>
#include <stdlib.h>
#include <string.h>

#include <hed.h>

#include <term/term.h>
#include <ui/core.h>
#include <ui/inputline.h>
#include <ui/fileshow.h>
#include <util/lists.h>
#include <main.h>


#define CMDBUF_SIZE 512

struct inputline_priv {
	const char *prefix;
	char buf[CMDBUF_SIZE];
	int buf_len;
	int cursor;

	int terminus;

	inputline_hook enter_hook;
	void *enter_hook_data;
};

static int inputline_y = 0;

static enum handler_result inputline_handler(struct ui_component *, const struct ui_event *);
static void inputline_redraw(struct ui_component *comp);

/* Initialize the component. */
void
inputline_init(const char *prefix, inputline_hook enter_hook, void *enter_hook_data)
{
	struct inputline_priv *data;
	struct ui_component *inputline;

	data = calloc(1, sizeof(struct inputline_priv));
	if (!data) goto nomem;
	data->prefix = prefix;
	data->enter_hook = enter_hook;
	data->enter_hook_data = enter_hook_data;

	inputline = ui_register(inputline_handler, data);
	if (!inputline) {
		free(data);
nomem:
		sched_exit(EX_OSERR);
		return;
	}

	inputline_y = term_get_max_y() - 1;

	inputline_redraw(inputline);
}

static void
inputline_done(struct ui_component *comp)
{
	free(comp->data);
	ui_unregister(comp);

	/* Visual cleanup. */
	term_clear_line(inputline_y);
	ui_redraw(); /* Restores cursor. */
}


static void
inputline_redraw(struct ui_component *comp)
{
	struct inputline_priv *data = comp->data;
	int pl = strlen(data->prefix); /* TODO: Cache on startup? */

	assert(data->buf_len >= 0 && data->buf_len < CMDBUF_SIZE
	       && data->buf[data->buf_len] == '\0');

	term_clear_line(inputline_y);
	term_print_string(0, inputline_y, COLOR_STATUS, data->prefix);
	term_print_string(pl, inputline_y, COLOR_STATUS, data->buf);
	cursor_x = pl + data->cursor;
	cursor_y = inputline_y;
}


static enum handler_result
inputline_in(struct inputline_priv *data, int ch)
{
	switch (ch) {
		case KEY_ENTER:
		case '\n':
		case '\r':
			if (data->buf_len) {
				static struct inputline_data idata;
				idata.buf = data->buf;
				idata.buf_len = data->buf_len;
				data->enter_hook(&idata, data->enter_hook_data);
			}
			/* Fall through. */
		case '\033':
cancel:
			data->buf[0] = 0;
			data->buf_len = 0;
			data->terminus = 1;
			return EVH_HOLD;

		case KEY_LEFT:
			if (data->cursor > 0)
				data->cursor--;
			return EVH_HOLD;
		case KEY_RIGHT:
			if (data->cursor < data->buf_len)
				data->cursor++;
			return EVH_HOLD;
		case KEY_HOME:
			data->cursor = 0;
			return EVH_HOLD;
		case KEY_END:
			data->cursor = data->buf_len;
			return EVH_HOLD;

		case KEY_BACKSPACE:
			if (data->cursor <= 0)
				goto cancel;
			assert(data->buf_len > 0);
			memmove(data->buf + data->cursor - 1, data->buf + data->cursor, data->buf_len - data->cursor + 1);
			data->cursor--, data->buf_len--;
			return EVH_HOLD;

		case KEY_DC:
			if (data->cursor == data->buf_len) {
				if (data->buf_len == 0)
					goto cancel;
				return EVH_HOLD;
			}
			assert(data->cursor < data->buf_len);
			memmove(data->buf + data->cursor, data->buf + data->cursor + 1, data->buf_len - data->cursor + 1);
			data->buf_len--;
			return EVH_HOLD;

		default:
			if (data->buf_len < CMDBUF_SIZE - 1) {
				memmove(data->buf + data->cursor + 1, data->buf + data->cursor, data->buf_len - data->cursor + 1);
				data->buf[data->cursor++] = ch;
				data->buf_len++;
			}
			return EVH_HOLD;
	}
}

static enum handler_result
inputline_handler(struct ui_component *comp, const struct ui_event *event)
{
	struct inputline_priv *data = comp->data;
	enum handler_result res = EVH_PASS;

	switch (event->type) {
		case EV_REDRAW:
			inputline_redraw(comp);
			break;
		case EV_KEYBOARD:
			res = inputline_in(data, event->v.keyboard.ch);
			if (res == EVH_HOLD) inputline_redraw(comp);
			if (data->terminus) inputline_done(comp);
			break;
		case EV_DONE:
			inputline_done(comp);
			break;
		default:
			assert(0);
			break;
	}

	return res;
}
