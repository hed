/* $Id$ */

/*
 * hed - Hexadecimal editor
 * Copyright (C) 2004  Petr Baudis <pasky@ucw.cz>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of version 2 of the GNU General Public License as
 * published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * As Beren looked into her eyes
 * Within the shadows of her hair,
 * The trembling starlight of the skies
 * He saw there mirrored shimmering.
 * Tinuviel the elven-fair,
 * Immortal maiden elven-wise,
 * About him cast her shadowy hair
 * And arms like silver glimmering.
 */

/* Feature macros needed for:
 *  - stpcpy
 */
#define _GNU_SOURCE

#include <config.h>

#include <ctype.h>
#include <curses.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>

#include <hed.h>

#include <config/config.h>
#include <libhed/expr.h>
#include <libhed/file.h>
#include <term/term.h>
#include <ui/core.h>
#include <ui/error.h>
#include <ui/fileshow.h>
#include <ui/inputline.h>
#include <util/lists.h>
#include <main.h>

/* For now, we re-request the file fragment each time we redraw the thing.
 * This might not be feasible in the future. */

/* Number of bytes to fetch at cursor position.
 * Currently, at maximum 4 bytes are needed (for the s32/u32 display).
 * If you need more in print_fileview_status, feel free to increase
 * this constant and use the extra bytes.
 */
#define CURSOR_LENGTH	4

/* TODO: Make the multiclipboard smarter - copy-on-write. */
/* TODO: Registers should work as in vim, esp. the unnamed (") register */
struct clipboard_register {
	unsigned char *data;
	hed_uoff_t len;
};
static struct clipboard_register clipboard[1 + 'z' - 'a' + 1 + 'Z' - 'A' + 1 + '9' - '0' + 1];

static inline char
ch2mark(char ch)
{
	return ('A' <= ch && ch <= 'Z') ? ch - 'A' :
	       ('a' <= ch && ch <= 'z') ? 26 + ch - 'a' :
	       -1;
}

static inline char
ch2reg(char ch)
{
	return ('0' <= ch && ch <= '9') ? 52 + ch - '0' :
		(ch == '"') ? 62 :
		ch2mark(ch);
}

static bool subst_all;

#define MARK_NR	('z' - 'a' + 1 + 'Z' - 'A' + 1)

struct fileshow_priv {
	struct hed_file *file;
	hed_cursor_t offset;	/* Beginning of screen (file offset) */

	int width, height;	/* Width and height of the viewport */
	ssize_t ssize;		/* Size (in bytes) of the viewport */

	hed_cursor_t cursor;	/* Cursor position (file offset) */
	hed_cursor_t visual;	/* Visual mark */
	int digitpos; /* Selected digit in the hexadecimal column. */
	unsigned char curbyte; /* The value of the current byte under the cursor. */

	hed_cursor_t insert;

	char idmark;		/* insertion/deletion mark at screen start */

	hed_cursor_t search;	/* Current search position */
	struct hed_expr *last_search;
	enum { SDIR_FORWARD = 1, SDIR_BACK = -1 } last_search_dir;

	enum { FSC_HEX, FSC_ASC } column;
	enum ui_mode mode;

	int reg;

	int le;			/* non-zero if little endian view */

	hed_cursor_t marks[MARK_NR];
};

static const char idmarks[] = {
	'>', ' ', '<',		/* insert start, normal, insert end */
	')', '|', '(',		/* dtto after erase */
};

#define IDMARK_ERASE_IDX	3

static enum handler_result fileshow_handler(struct ui_component *, const struct ui_event *);
static long eval_reg_cb(void *hookdata, char reg, hed_uoff_t ofs,
			unsigned char *scramble, size_t len);
static long eval_mark_cb(void *hookdata, char mark,
			 unsigned char *scramble, size_t len);

enum ui_mode opt_defmode = -1;

static void
update_idmark(struct fileshow_priv *data)
{
	int idx = 1;

	/* marks can only happen at zero offset */
	if (!data->offset.off) {
		idx = !hed_block_is_inserted(data->offset.block);
		if (hed_block_is_after_erase(data->offset.block))
			idx += IDMARK_ERASE_IDX;
		if (hed_block_is_after_insert(data->offset.block))
			idx = 1;
	}
	data->idmark = idmarks[idx];
}

static unsigned
pos_column(struct fileshow_priv *data, hed_uoff_t pos)
{
	return pos % data->width;
}

static void
set_viewport(struct fileshow_priv *data, hed_uoff_t pos)
{
	hed_update_cursor(data->file, pos, &data->offset);
	update_idmark(data);
}

/* Update screen offset to make cursor fit into the current viewport */
static void
fixup_viewport(struct fileshow_priv *data)
{
	hed_uoff_t curline = data->cursor.pos -
		pos_column(data, data->cursor.pos);

	if (curline < data->offset.pos)
		set_viewport(data, curline);
	else if (curline >= data->offset.pos + data->ssize)
		set_viewport(data, curline - data->ssize + data->width);
}

static void
set_cursor(struct fileshow_priv *data, hed_uoff_t pos)
{
	hed_update_cursor(data->file, pos, &data->cursor);
	fixup_viewport(data);
}


/* Update cursor position to fit into the current viewport */
static void
fixup_cursor(struct fileshow_priv *data)
{
	hed_uoff_t curline = data->cursor.pos -
		pos_column(data, data->cursor.pos);

	if (curline < data->offset.pos)
		hed_move_relative(&data->cursor,
				  data->offset.pos - curline);
	else if (curline >= data->offset.pos + data->ssize)
		hed_move_relative(&data->cursor,
				  data->offset.pos + data->ssize -
				  data->width - curline);
}

static void
slide_viewport(struct fileshow_priv *data, hed_off_t num)
{
	hed_move_relative(&data->offset, num);
	update_idmark(data);
	fixup_cursor(data);
}

/* Initialize the component. */
void
fileshow_init(struct hed_file *file)
{
	struct fileshow_priv *data;
	struct ui_component *fileshow;
	int width;

	data = calloc(1, sizeof(struct fileshow_priv));
	if (!data) goto nomem;
	data->file = file;
	data->reg = ch2reg('"');
	data->le = arch_little_endian();

	if ((int) opt_defmode >= 0) {
		data->mode = opt_defmode;
	} else {
		char *defmode = get_opt_str("default_mode", "normal");
		if (!strcmp(defmode, "normal")) {
			data->mode = MODE_NORMAL;
		} else if (!strcmp(defmode, "insert")) {
			data->mode = MODE_INSERT;
		} else if (!strcmp(defmode, "replace")) {
			data->mode = MODE_REPLACE;
		} else {
			data->mode = MODE_NORMAL;
		}
	}

	fileshow = ui_register(fileshow_handler, data);
	if (!fileshow) {
		free(data);
nomem:
		sched_exit(EX_OSERR);
		return;
	}

	/* TODO: Better height calculation. */
	data->height = term_get_max_y() - 2;
	width = (term_get_max_x() - /* ofs */ 8 - /* spaces */ 4);
	width /= 4;
	if (!opt_spreadview) {
		width = 1 << bitsize(width);
		width = get_opt_int("bytes_per_line", width);
	}
	data->width = width;
	data->ssize = data->height * data->width;

	hed_get_cursor(data->file, 0, &data->cursor);
	hed_dup_cursor(&data->cursor, &data->offset);
	data->idmark = ' ';
}

static void
fileshow_done(struct ui_component *comp)
{
	free(comp->data);
	ui_unregister(comp);
}

/* Check whether @pos is inside the visual block */
static int
is_inside_visual(struct fileshow_priv *data, hed_uoff_t pos)
{
	return (data->cursor.pos <= pos && pos <= data->visual.pos) ||
		(data->visual.pos <= pos && pos <= data->cursor.pos);
}

/* Returns ZERO ON ERROR. */
static size_t
visual_extents(struct fileshow_priv *data, hed_cursor_t *base)
{
	hed_cursor_t *basesrc;
	size_t ret;
	if (data->visual.pos < data->cursor.pos) {
		ret = data->cursor.pos - data->visual.pos + 1;
		basesrc = &data->visual;
	} else {
		ret = data->visual.pos - data->cursor.pos + 1;
		basesrc = &data->cursor;
	}
	hed_dup2_cursor(basesrc, base);
	return ret;
}

#define BYTES_PER_LONG	(sizeof(unsigned long))

#define DIGITS_MAX(num)	((num + 9)/10)

/* The decadic logarithm of 2^8 is approx. 2.408239965311849584,
 * so 3 digits must be always sufficient to represent the largest
 * value that can be held in one byte. */
#define DIGITS_PER_BYTE	3

static int
print_status_num(unsigned char *cursdata, int nbytes,
		 int little_endian, int x, int y, int width)
{
	hed_uoff_t num;
	long flags;

	flags = little_endian ? HED_AEF_FORCELE : HED_AEF_FORCEBE;

	if (nbytes < 0) {
		nbytes = -nbytes;
		flags |= HED_AEF_SIGNED;
	}

	assert(x && cursdata);
	assert(nbytes * 8 <= _FILE_OFFSET_BITS);

	num = hed_bytestr2off(cursdata, nbytes, flags);

	return term_printf(x, y, COLOR_STATUS, flags & HED_AEF_SIGNED
		      ? "s%d:%*lld "
		      : "u%d:%*llu ",
		      nbytes * 8, width, (long long) num);
}

static void
print_fileview_status(struct fileshow_priv *data,
		      unsigned char *cursdata, int ofslen)
{
	static char modechar[] = { 'N', 'R', 'I', 'V' };
	int pos = 0;
	const int y = data->height;

	/* First, fill the status line with the status color */
	term_set_bg_color(COLOR_STATUS);
	term_clear_line(data->height);

	/* We print the filename first so that it gets overwritten by
	 * the "more important" stuff. The most important part of the
	 * filename tends to be at the end anyway. */
	term_print_string(term_get_max_x() - 1 - strlen(data->file->name),
	                  y, COLOR_STATUS, data->file->name);

	pos += term_printf(0, y, COLOR_STATUS, "%0*llx: %03d  ", ofslen,
			   (unsigned long long) data->cursor.pos, *cursdata);

	term_print_string(pos, y, COLOR_STATUS, data->le ? "LE: " : "BE: ");
	pos += 4;

	pos += print_status_num(cursdata, 2, data->le, pos, y, 6);
	pos += print_status_num(cursdata, -2, data->le, pos, y, 6);
	pos += print_status_num(cursdata, 4, data->le, pos, y, 11);
	pos += print_status_num(cursdata, -4, data->le, pos, y, 11);

	term_print_char(pos, y, COLOR_MODE,
			hed_file_is_modified(data->file) ? '*' : ' ');
	term_print_char(pos + 1, y, COLOR_MODE, modechar[data->mode]);
}


static int
num_hex_width(hed_uoff_t num)
{
	/* Offset of the leftmost non-zero bit. */
	int width = 0;
	int i;

	for (i = sizeof(hed_uoff_t) * 8 / 2; i > 0; i /= 2) {
		if (num & (~(hed_uoff_t)0 << i)) {
			num >>= i; width += i;
		}
	}
	/* Round the width up to the hexadecimal places. */
	return width / 4 + 1;
}

static void
fileshow_redraw(struct ui_component *comp)
{
	struct fileshow_priv *data = comp->data;
	hed_cursor_t pos;
	char idmark;		/* insertion/deletion marks */

	hed_uoff_t maxofs;
	int ofslen;
	int xindent, xascpos;
	int row;

	unsigned char *p;
	unsigned char cursdata[CURSOR_LENGTH];

	if (!hed_prepare_read(data->file, &data->offset, 1)) {
		/* This is a fatal error (e.g. no memory) */
		errmsg("Cannot display file content");
		return;
	}

	/* Offset width is chosen to accomodate the whole file in its current
	 * size as well as the current screen. */
	maxofs = data->offset.pos + data->ssize - 1;
	if (maxofs < hed_file_size(data->file))
		maxofs = hed_file_size(data->file);
	ofslen = num_hex_width(maxofs);
	/* Always print at least 4 digits. */
	if (ofslen < 4)
		ofslen = 4;
	xindent = ofslen + 2;
	xascpos = xindent + data->width * 3 + 1;

	hed_dup_cursor(&data->offset, &pos);
	idmark = data->idmark;
	assert(pos.pos >= 0);

	term_set_bg_color(COLOR_NEUTRAL);
	erase();

	p = hed_cursor_data(&pos);

	for (row = 0; row < data->height; ++row) {
		int col;
		term_color oldv = 0;

		term_printf(0, row, COLOR_OFFSET, "%0*llx", ofslen, pos.pos);

		for (col = 0; col < data->width; ++col) {
			char hex[3];
			unsigned char asc;
			term_color color, v;

			v = (data->mode == MODE_VISUAL &&
			     is_inside_visual(data, pos.pos))
				? COLOR_VISUAL
				: 0;

			color = COLOR_DATA_BASE;
			if (pos.pos >= hed_file_size(data->file))
				color |= COLOR_DATA_OUTSIDE;

			term_color markcolor = (idmark == ' ')
				? (hed_block_is_dirty(pos.block)
				   ? COLOR_DIRTY
				   : color)
				: COLOR_MARK;
			term_print_char(xindent + col * 3 - 1, row,
					markcolor | v | oldv, idmark);

			if (pos.pos == data->cursor.pos) {
				cursor_y = row;
				cursor_x = data->column == FSC_HEX
					? xindent + col * 3 + data->digitpos
					: xascpos + col;
				color |= COLOR_DATA_CURSOR;
			} else if (hed_block_is_dirty(pos.block))
				color = COLOR_DIRTY;
			if (hed_block_is_bad(pos.block))
				color = COLOR_ERROR;
			color |= v;

			asc = p ? *p++ : 0x00;
			sprintf(hex,
				hed_block_is_bad(pos.block) ? "XX" : "%02x",
				asc);
			asc = isprint(asc) ? asc : asc == '\0' ? '.' : ':';

			term_print_string(xindent + col * 3, row,
					  color, hex);
			if (color >= COLOR_DATA_BASE)
				color |= COLOR_DATA_ASCII;
			term_print_char(xascpos + col, row,
					color, (hed_block_is_bad(pos.block)
						? '?' : asc));

			int idx = !!hed_block_is_inserted(pos.block);
			hed_move_relative(&pos, 1);
			if (!pos.off) {
				idx -= !!hed_block_is_inserted(pos.block);
				if (hed_block_is_after_erase(pos.block))
					idx += IDMARK_ERASE_IDX;
				idmark = idmarks[idx + 1];

				if (!hed_prepare_read(data->file, &pos, 1)) {
					/* This is a fatal error */
					errmsg("Cannot display file content");
					return;
				}

				p = hed_cursor_data(&pos);
			} else
				idmark = ' ';

			oldv = v;
		}

		if (idmark == '<' || idmark == '(') {
			term_print_char(xascpos - 2, row,
					COLOR_MARK | oldv, idmark);
			idmark = ' ';
		} else if (oldv)
			term_print_char(xascpos - 2, row,
					COLOR_NEUTRAL | COLOR_VISUAL, ' ');
	}

	hed_put_cursor(&pos);

	assert(data->cursor.pos >= 0);

	memset(cursdata, 0, sizeof(cursdata));
	hed_file_cpin(data->file, cursdata, sizeof(cursdata), &data->cursor);
	data->curbyte = cursdata[0];

	print_fileview_status(data, cursdata, ofslen);
	term_unset_bg_color();
}

/* Jump to a position given by offset expression. */
static void
jump_offset(struct inputline_data *inpline, void *hookdata)
{
	struct fileshow_priv *data = hookdata;
	struct hed_expr *expr;
	hed_uoff_t pos;

	expr = hed_expr_new(inpline->buf, eval_reg_cb, eval_mark_cb, data);
	if (!expr) {
		errmsg("Invalid expression");
		return;
	}
	if (hed_expr_eval(expr) & HED_AEF_ERROR) {
		errmsg("Cannot evaluate expression");
		hed_expr_free(expr);
		return;
	}
	pos = hed_expr2off(expr);
	hed_expr_free(expr);

	set_cursor(data, pos);
}

static int
jump_relative(struct fileshow_priv *data, int num)
{
	/* Zero-length jump usually means an invalid jump out of bounds */
	if (hed_move_relative(&data->cursor, num) == 0)
		return -1;

	data->digitpos = 0;
	fixup_viewport(data);
	return 0;
}

static void
finish_insert(struct fileshow_priv *data)
{
	if (hed_is_a_cursor(&data->insert)) {
		hed_file_insert_end(data->file, &data->insert);
		if (data->column == FSC_HEX && data->digitpos)
			jump_relative(data, 1);
	}
}

static void
erase_at_cursor(struct fileshow_priv *data, hed_uoff_t len)
{
	hed_file_erase_block(data->file, &data->cursor, len);
	if (!hed_is_a_cursor(&data->offset)) {
		hed_dup2_cursor(&data->cursor, &data->offset);
		hed_move_relative(&data->offset,
				  -pos_column(data, data->offset.pos));
		update_idmark(data);
	}
}

static hed_uoff_t
do_search(struct fileshow_priv *data)
{
	int res;

	hed_dup_cursor(&data->cursor, &data->search);
	hed_move_relative(&data->search, data->last_search_dir);
	res = hed_file_find_expr(data->file, &data->search,
				 data->last_search_dir, data->last_search);
	if (!res)
		set_cursor(data, data->search.pos);
	else if (!subst_all)
		errmsg("No match found");

	hed_put_cursor(&data->search);
	return data->search.pos;
}

/* Search for the given string. */
static void
expr_search(struct inputline_data *inpline, void *hookdata)
{
	struct fileshow_priv *data = hookdata;

	if (data->last_search)
		hed_expr_free(data->last_search);
	data->last_search = hed_expr_new(inpline->buf,
					 eval_reg_cb, eval_mark_cb, data);
	if (!data->last_search) {
		errmsg("Invalid expression");
		return;
	}

	do_search(data);
}

/* Search and substitute the given string. */
static void
expr_subst(struct inputline_data *inpline, void *hookdata)
{
	struct fileshow_priv *data = hookdata;
	struct hed_expr *subst;

	subst = hed_expr_new(inpline->buf, eval_reg_cb, eval_mark_cb, data);
	if (!subst) {
		errmsg("Invalid expression");
		return;
	}

	data->last_search_dir = SDIR_FORWARD;
	while (do_search(data) != HED_FINDOFF_NO_MATCH) {
		if (hed_expr_eval(subst) & HED_AEF_ERROR) {
			errmsg("Cannot evaluate expression");
			break;
		}
		/* Cursor at the match */
		erase_at_cursor(data, hed_expr_len(data->last_search));
		hed_file_insert_once(data->file, &data->cursor,
				     hed_expr_buf(subst), hed_expr_len(subst));
		if (!subst_all)
			break;
	}
	hed_expr_free(subst);
}

static void
expr_subst1(struct inputline_data *inpline, void *hookdata)
{
	struct fileshow_priv *data = hookdata;

	if (data->last_search)
		hed_expr_free(data->last_search);
	data->last_search = hed_expr_new(inpline->buf,
					 eval_reg_cb, eval_mark_cb, data);
	if (!data->last_search) {
		errmsg("Invalid expression");
		return;
	}
	inputline_init("s/.../", expr_subst, data);
}

static void
clip_yank(struct fileshow_priv *data)
{
	hed_cursor_t base = HED_NULL_CURSOR;
	size_t len = visual_extents(data, &base);
	void *buf = realloc(clipboard[data->reg].data, len);

	if (buf) {
		clipboard[data->reg].data = buf;
		len -= hed_file_cpin(data->file, buf, len, &base);
		clipboard[data->reg].len = len;
	}
	hed_put_cursor(&base);
}

/* Those functions are totally horrible kludges. There should be specialized
 * file methods for those operations. */

static void
clip_delete(struct fileshow_priv *data)
{
	size_t len;

	clip_yank(data);
	len = visual_extents(data, &data->cursor);
	erase_at_cursor(data, len);
}

static void
clip_put(struct fileshow_priv *data)
{
	if (!clipboard[data->reg].data) {
		errmsg("Register empty");
		return;
	}
	hed_file_insert_once(data->file, &data->cursor,
			     clipboard[data->reg].data,
			     clipboard[data->reg].len);
	if (data->offset.pos == data->cursor.pos)
		hed_move_relative(&data->offset,
				  -clipboard[data->reg].len);
}

static void
clip_overwrite(struct fileshow_priv *data)
{
	hed_cursor_t start = HED_NULL_CURSOR;
	hed_uoff_t i;
	size_t len;

	if (!clipboard[data->reg].data) {
		errmsg("Register empty");
		return;
	}

	if (data->mode == MODE_VISUAL) {
		/* spill */
		len = visual_extents(data, &start);
	} else {
		/* splat */
		hed_dup_cursor(&data->cursor, &start);
		len = clipboard[data->reg].len;
	}

	i = min(len, clipboard[data->reg].len);
	hed_file_set_block(data->file, &start, clipboard[data->reg].data, i);
	hed_file_set_bytes(data->file, &start, 0, len - i);
	hed_put_cursor(&start);
}

static void
clip_swap(struct fileshow_priv *data)
{
	struct clipboard_register orig, new;

	new = clipboard[data->reg];
	clipboard[data->reg].data = NULL;
	clip_delete(data);
	orig = clipboard[data->reg];
	clipboard[data->reg] = new;
	clip_put(data);
	clipboard[data->reg] = orig;
}


static void
set_width(struct inputline_data *inpline, void *hookdata)
{
	struct fileshow_priv *data = hookdata;
	char *l;
	int w;
	w = strtol(inpline->buf, &l, 0);
	if (*l) {
		errmsg("Invalid width");
		return;
	}
	data->width = w;
}


static void
read_region(struct inputline_data *inpline, void *hookdata)
{
	struct fileshow_priv *data = hookdata;
	char *fname = inpline->buf;
	int flen = inpline->buf_len;
	FILE *f;
	int ch;

	/* Trim leading/trailing whitespaces */
	while (isspace(*fname))
		fname++, flen--;
	while (isspace(fname[flen - 1]) && flen > 0)
		flen--;
	if (!*fname)
		return;
	f = fopen(fname, "r");
	if (!f) {
		errmsg(strerror(errno));
		return;
	}

	if (data->mode == MODE_VISUAL) {
		size_t len = visual_extents(data, &data->cursor);
		erase_at_cursor(data, len);
	}

	/* Ugh. :-) */
	hed_file_insert_begin(data->file, &data->cursor, &data->insert);
	while ((ch = fgetc(f)) != EOF)
		hed_file_insert_byte(data->file, &data->insert, ch);
	finish_insert(data);
	fclose(f);
}

static int
write_to_file(struct hed_file *in, hed_cursor_t *pos, hed_uoff_t len,
	      FILE *out)
{
	while (len) {
		size_t blen, written;
		void *p;

		if (! (blen = hed_prepare_read(in, pos, len)) )
			return -1;

		if ( (p = hed_cursor_data(pos)) )
			written = fwrite(p, 1, blen, out);
		else for (written = 0; written < blen; ++written)
			     if (fputc(0, out) < 0)
				     break;

		hed_move_relative(pos, written);
		if (written < blen)
			return -1;
		len -= blen;
	}
	return 0;
}

static void
write_region(struct inputline_data *inpline, void *hookdata)
{
	struct fileshow_priv *data = hookdata;
	hed_cursor_t base = HED_NULL_CURSOR;
	size_t len;
	char *fname = inpline->buf;
	int flen = inpline->buf_len;
	FILE *f;

	/* Trim leading/trailing whitespaces */
	while (isspace(*fname))
		fname++, flen--;
	while (isspace(fname[flen - 1]) && flen > 0)
		flen--;
	if (!*fname)
		return;

	f = fopen(fname, "w");
	if (!f) {
		errmsg(strerror(errno));
		return;
	}
	len = visual_extents(data, &base);
	if (write_to_file(data->file, &base, len, f))
		errmsg(strerror(errno));
	hed_put_cursor(&base);
	fclose(f);
}

/* Arbitrarily chosen... */
#define CHUNK_SIZE	1024

static int
filter_select(struct fileshow_priv *data, hed_cursor_t *base,
	      ssize_t len, int fd_in, int fd_out)
{
	fd_set fdr, fdw;
	int ret;

	hed_file_insert_begin(data->file, &data->cursor, &data->insert);
	while (fd_out || fd_in) {
		FD_ZERO(&fdr);
		FD_ZERO(&fdw);
		if (fd_in) FD_SET(fd_in, &fdr);
		if (fd_out) FD_SET(fd_out, &fdw);

		ret = select(fd_in+fd_out+1, &fdr, &fdw, NULL, NULL);

		if (ret <= 0)
			continue;

		if (FD_ISSET(fd_in, &fdr)) {
			unsigned char buf[CHUNK_SIZE];
			ret = read(fd_in, buf, CHUNK_SIZE);
			if (ret < 0)
				goto err;
			if (ret == 0) {
				close(fd_in);
				fd_in = 0;
			}
			hed_file_insert_block(data->file, &data->insert,
					      buf, ret);
			data->cursor.pos += ret;
		}

		if (FD_ISSET(fd_out, &fdw)) {
			size_t blen = min(len, CHUNK_SIZE);
			unsigned char *buf = malloc(blen);
			blen -= hed_file_cpin(data->file, buf, blen, base);
			if (blen < CHUNK_SIZE) {
				len = 0;
			} else {
				len -= blen;
			}
			ret = write(fd_out, buf, blen);
			free(buf);
			if (ret <= 0)
				goto err;
			if (ret < blen)
				len += blen - ret;
			hed_move_relative(base, ret - blen);

			if (!len) {
				close(fd_out);
				fd_out = 0;
			}
		}
	}
	finish_insert(data);
	return 0;
err:
	finish_insert(data);
	if (fd_out)
		close(fd_out);
	if (fd_in)
		close(fd_in);
	return -1;
}

static void
pipe_region(struct inputline_data *inpline, void *hookdata)
{
	struct fileshow_priv *data = hookdata;
	hed_cursor_t base = HED_NULL_CURSOR;
	size_t len = visual_extents(data, &base);
	char *argv[4] = { "/bin/sh", "-c", inpline->buf, NULL };
	int p_rd[2], p_wr[2];
	pid_t pid;

	hed_put_cursor(&data->visual);
	data->mode = MODE_NORMAL;

	pipe(p_rd);
	pipe(p_wr);

	pid = fork();
	if (pid == 0) {
		close(p_rd[1]);
		close(p_wr[0]);

		dup2(p_rd[0], 0);
		dup2(p_wr[1], 1);

		exit(execvp(argv[0], argv));

	} else if (pid > 0) {
		int status, ret;

		close(p_rd[0]);
		close(p_wr[1]);

		set_cursor(data, base.pos + len);
		if (filter_select(data, &base, len, p_wr[0], p_rd[1]) < 0) {
			errmsg("Filter failed");
			hed_put_cursor(&base);
			return;
		}
		ret = waitpid(pid, &status, 0);
		if (ret != pid) {
			errmsg("Funny zombie");
			hed_put_cursor(&base);
			return;
		}
		if (WIFEXITED(status)) {
			if (WEXITSTATUS(status))
				errmsg("Filter returned error");
			set_cursor(data, base.pos);
			erase_at_cursor(data, len);
			hed_put_cursor(&base);
			return;
		}
		errmsg("Curious error");
		hed_put_cursor(&base);
		return;

	} else {
		errmsg("Fork failed");
		hed_put_cursor(&base);
		return;
	}
	hed_put_cursor(&base);
}

static hed_cursor_t xre;

/* Returns true iff @mark is a valid mark */
static inline bool
mark_is_valid(struct fileshow_priv *data, int mark)
{
	return hed_is_a_cursor(&data->marks[mark]);
}

static long
eval_mark_cb(void *hookdata, char mark,
	     unsigned char *scramble, size_t len)
{
	struct fileshow_priv *data = hookdata;
	int m = ch2mark(mark);
	if (m >= 0) {
		if (!mark_is_valid(data, m))
			return HED_AEF_ERROR;
		hed_off2bytestr(scramble, len, data->marks[m].pos);
	} else if (mark == '$') {
		hed_off2bytestr(scramble, len, data->cursor.pos);
	} else if (mark == '.' && hed_is_a_cursor(&data->search)) {
		hed_off2bytestr(scramble, len, data->search.pos);
		return HED_AEF_DYNAMIC;
	} else
		return HED_AEF_ERROR;

	return 0;
}


static long
eval_reg_cb(void *hookdata, char reg, hed_uoff_t ofs,
	    unsigned char *scramble, size_t len)
{
	struct fileshow_priv *data = hookdata;
	unsigned char *buf;
	size_t blen;
	hed_cursor_t blkpos;
	hed_off_t skip;
	int r;
	long ret = 0;

	switch (reg) {
	case '$':
		hed_dup_cursor(&data->cursor, &blkpos);
		break;
	case '_':
		hed_get_cursor(data->file, 0, &blkpos);
		break;
	case '.':
		if (hed_is_a_cursor(&data->search)) {
			hed_dup_cursor(&data->search, &blkpos);
			ret |= HED_AEF_DYNAMIC;
		} else if (hed_is_a_cursor(&xre))
			hed_dup_cursor(&xre, &blkpos);
		else
			goto out_zero;
		break;
	default:
		r = ch2reg(reg);
		if (r < 0)
			goto out_zero;
		buf = clipboard[r].data; blen = clipboard[r].len;
		if (ofs < 0) {
			if (-ofs >= len)
				goto out_zero;
			memset(scramble, 0, -ofs);
			scramble -= ofs;
			len += ofs;
		} else {
			if (ofs >= blen)
				goto out_zero;
			buf += ofs;
			blen -= ofs;
		}
		if (blen > len)
			blen = len;
		memcpy(scramble, buf, blen);
		if (blen < len)
			memset(scramble + blen, 0, len - blen);
		return 0;
	}

	skip = hed_move_relative(&blkpos, ofs) - ofs;
	if (skip) {
		if (ofs >= 0 || skip >= len)
			goto out_zero;
		memset(scramble, 0, skip);
		scramble += skip;
		len -= skip;
	}
	if (hed_file_cpin(data->file, scramble, len, &blkpos))
		ret |= HED_AEF_ERROR;
	hed_put_cursor(&blkpos);
	return ret;

 out_zero:
	memset(scramble, 0, len);
	return 0;
}


static void
ieval_expr(struct inputline_data *inpline, void *hookdata)
{
	struct fileshow_priv *data = hookdata;
	struct hed_expr *expr;

	expr = hed_expr_new(inpline->buf, eval_reg_cb, eval_mark_cb, data);
	if (!expr) {
		errmsg("Invalid expression");
		return;
	}
	if (hed_expr_eval(expr) & HED_AEF_ERROR) {
		errmsg("Cannot evaluate expression");
		hed_expr_free(expr);
		return;
	}
	hed_file_insert_once(data->file, &data->cursor,
			     hed_expr_buf(expr), hed_expr_len(expr));
	hed_expr_free(expr);
}

static void
peval_expr(struct inputline_data *inpline, void *hookdata)
{
	struct fileshow_priv *data = hookdata;
	struct hed_expr *expr;
	static char *resbuf;
	char *p;
	size_t i, len;
	unsigned char *buf;

	expr = hed_expr_new(inpline->buf, eval_reg_cb, eval_mark_cb, data);
	if (!expr) {
		errmsg("Invalid expression");
		return;
	}
	if (hed_expr_eval(expr) & HED_AEF_ERROR) {
		errmsg("Cannot evaluate expression");
		hed_expr_free(expr);
		return;
	}
	len = hed_expr_len(expr);
	p = realloc(resbuf, sizeof("Result: -") + 3 * len);
	if (!p) {
		errmsg("Cannot allocate buffer");
		return;
	}

	resbuf = p;
	p = stpcpy(resbuf, "Result:");
	if (hed_expr_flags(expr) & HED_AEF_NEGATIVE)
		p = stpcpy(p, " -");
	for (i = 0, buf = hed_expr_buf(expr); i < len; ++i)
		p += sprintf(p, " %02x", buf[i]);
	hed_expr_free(expr);
	errmsg(resbuf);
}


static void
reval_expr(struct inputline_data *inpline, void *hookdata)
{
	struct fileshow_priv *data = hookdata;
	struct hed_expr *expr;
	int elen;
	size_t i, len;

	expr = hed_expr_new(inpline->buf, eval_reg_cb, eval_mark_cb, data);
	if (!expr) {
		errmsg("Invalid expression");
		goto out;
	}
	elen = hed_expr_len(expr);
	len = visual_extents(data, &xre);
	for (i = 0; i < len; i += elen) {
		size_t rlen;
		if (hed_expr_eval(expr) & HED_AEF_ERROR) {
			errmsg("Cannot evaluate expression");
			break;
		}
		rlen = elen;
		if (rlen > len - i)
			rlen = len - i;
		hed_file_set_block(data->file, &xre, hed_expr_buf(expr), rlen);
	}
	hed_expr_free(expr);
	hed_put_cursor(&xre);
	xre.block = NULL;
 out:
	if (data->mode != MODE_VISUAL)
		hed_put_cursor(&data->visual);
}

static void exmode(struct inputline_data *inpline, void *hookdata);

/* Evaluate an exmode command. */
static void
exmode_cmd_write(struct fileshow_priv *data)
{
	if (data->mode == MODE_VISUAL) {
		/* Write just the visual */
		inputline_init("File: ", write_region, data);
	} else
		hed_file_commit(data->file);
}
static void
exmode_cmd_read(struct fileshow_priv *data)
{
	inputline_init("File: ", read_region, data);
}
static void
exmode_cmd_pipe(struct fileshow_priv *data)
{
	inputline_init("Command: ", pipe_region, data);
}
static void
exmode_cmd_subst(struct fileshow_priv *data)
{
	subst_all = false;
	inputline_init("s/", expr_subst1, data);
}
static void
exmode_cmd_Subst(struct fileshow_priv *data)
{
	subst_all = true;
	inputline_init("S/", expr_subst1, data);
}
static void
exmode_cmd_swp(struct fileshow_priv *data)
{
	hed_file_write_swap(data->file);
}
static void
exmode_cmd_ieval(struct fileshow_priv *data)
{
	inputline_init("Expression: ", ieval_expr, data);
}
static void
exmode_cmd_reval(struct fileshow_priv *data)
{
	if (data->mode != MODE_VISUAL)
		hed_dup_cursor(&data->cursor, &data->visual);
	inputline_init("Expression: ", reval_expr, data);
}
static void
exmode_cmd_peval(struct fileshow_priv *data)
{
	inputline_init("Expression: ", peval_expr, data);
}
static void
exmode_cmd_width(struct fileshow_priv *data)
{
	inputline_init("Bytes per line: ", set_width, data);
}
static void
exmode_cmd_quit(struct fileshow_priv *data)
{
	terminus = true;
}
static void
exmode_cmd_exit(struct fileshow_priv *data)
{
	if (hed_file_is_modified(data->file))
		hed_file_commit(data->file);
	exmode_cmd_quit(data);
}

enum command {
	cmd_backspace,
	cmd_clip_delete,
	cmd_clip_overwrite,
	cmd_clip_paste,
	cmd_clip_paste_after,
	cmd_clip_yank,
	cmd_delchar,
	cmd_delchar_back,
	cmd_endian,
	cmd_exit,
	cmd_exmode,
	cmd_file_begin,
	cmd_file_end,
	cmd_go_down,
	cmd_go_left,
	cmd_go_right,
	cmd_goto,
	cmd_go_up,
	cmd_ieval,
	cmd_insert,
	cmd_line_begin,
	cmd_line_end,
	cmd_normal_mode,
	cmd_page_down,
	cmd_page_up,
	cmd_peval,
	cmd_pipe,
	cmd_prefix,
	cmd_quit,
	cmd_read,
	cmd_replace,
	cmd_reval,
	cmd_scroll_down,
	cmd_scroll_up,
	cmd_search,
	cmd_search_back,
	cmd_search_next,
	cmd_search_prev,
	cmd_subst,
	cmd_substall,
	cmd_switch_column,
	cmd_swp,
	cmd_visual,
	cmd_width,
	cmd_write
};

static enum handler_result
do_command(struct fileshow_priv *data, enum command cmd)
{
	switch (cmd) {

	/* CURSOR MOVEMENT */

	case cmd_go_down:
		jump_relative(data, data->width);
		break;

	case cmd_go_up:
		jump_relative(data, -data->width);
		break;

	case cmd_go_left:
		if (data->column == FSC_HEX && data->digitpos > 0) {
			data->digitpos--;
		} else if (!jump_relative(data, -1) &&
			   data->column == FSC_HEX)
			data->digitpos = 1;
		break;

	case cmd_go_right:
		if (data->column == FSC_HEX && data->digitpos < 1) {
			data->digitpos++;
		} else
			jump_relative(data, 1);
		break;

	case cmd_line_begin:
		if (!jump_relative(data, -pos_column(data, data->cursor.pos)))
			data->digitpos = 0;
		break;

	case cmd_line_end:
		if (!jump_relative(data, data->width -
				   pos_column(data, data->cursor.pos) - 1))
			data->digitpos = 0;
		break;

	case cmd_file_end:
		if (hed_file_size(data->file)) {
			set_cursor(data, hed_file_size(data->file) - 1);
			data->digitpos = 0;
			break;
		}
		/* else fall-through */

	case cmd_file_begin:
		set_cursor(data, 0);
		data->digitpos = 0;
		break;

	case cmd_page_down:
		slide_viewport(data, data->ssize - data->width);
		break;

	case cmd_page_up:
		slide_viewport(data, -data->ssize + data->width);
		break;

	case cmd_scroll_down:
		slide_viewport(data, data->width);
		break;

	case cmd_scroll_up:
		slide_viewport(data, -data->width);
		break;

	/* SIMPLE COMMANDS */

	case cmd_switch_column:
		data->column ^= 1;
		break;

	case cmd_visual:
		if (data->mode == MODE_VISUAL) {
			hed_put_cursor(&data->visual);
			data->mode = MODE_NORMAL;
		} else {
			data->mode = MODE_VISUAL;
			hed_dup_cursor(&data->cursor, &data->visual);
		}
		break;

	case cmd_normal_mode:
		if (data->mode == MODE_VISUAL)
			hed_put_cursor(&data->visual);
		data->mode = MODE_NORMAL;
		break;

	case cmd_insert:
		if (data->mode == MODE_INSERT)
			data->mode = MODE_REPLACE;
		else
			data->mode = MODE_INSERT;
		break;

	case cmd_replace:
		data->mode = MODE_REPLACE;
		break;

	case cmd_delchar:
		erase_at_cursor(data, 1);
		break;

	case cmd_delchar_back:
		if (data->cursor.pos > 0) {
			jump_relative(data, -1);
			erase_at_cursor(data, 1);
		}
		break;

	case cmd_backspace:
		return do_command(data, (cmd = data->mode == MODE_INSERT
					 ? cmd_delchar_back : cmd_go_left));

	/* CLIPBOARD */

	case cmd_clip_yank:
		if (data->mode != MODE_VISUAL) {
			errmsg("No region selected");
			return EVH_SILENTHOLD;
		}
		clip_yank(data);
		hed_put_cursor(&data->visual);
		data->mode = MODE_NORMAL;
		break;

	case cmd_clip_delete:
		if (data->mode != MODE_VISUAL) {
			errmsg("No region selected");
			return EVH_SILENTHOLD;
		}
		clip_delete(data);
		hed_put_cursor(&data->visual);
		data->mode = MODE_NORMAL;
		break;

	case cmd_clip_paste_after:
		if (data->mode != MODE_VISUAL)
			hed_move_relative(&data->cursor, 1);
		/* fall through */
	case cmd_clip_paste:
		if (data->mode == MODE_VISUAL) {
			clip_swap(data);
			hed_put_cursor(&data->visual);
			data->mode = MODE_NORMAL;
		} else {
			clip_put(data);
			hed_move_relative(&data->cursor, -1);
		}
		break;

	case cmd_clip_overwrite:
		clip_overwrite(data);
		if (data->mode == MODE_VISUAL) {
			hed_put_cursor(&data->visual);
			data->mode = MODE_NORMAL;
		}
		break;

	/* INPUTLINE */

	case cmd_exmode:
		inputline_init(":", exmode, data);
		return EVH_SILENTHOLD;

	case cmd_goto:
		inputline_init("#", jump_offset, data);
		return EVH_SILENTHOLD;

	case cmd_search:
		data->last_search_dir = SDIR_FORWARD;
		inputline_init("/", expr_search, data);
		return EVH_SILENTHOLD;

	case cmd_search_back:
		data->last_search_dir = SDIR_BACK;
		inputline_init("?", expr_search, data);
		return EVH_SILENTHOLD;

	case cmd_search_prev:
		data->last_search_dir = -data->last_search_dir;
		/* fall through */
	case cmd_search_next:
		if (data->last_search)
			do_search(data);
		else
			errmsg("No previous search");

		if (cmd == cmd_search_prev)
			data->last_search_dir = -data->last_search_dir;
		break;

	/* EXMODE */

	case cmd_quit:
		exmode_cmd_quit(data);
		break;

	case cmd_exit:
		exmode_cmd_exit(data);
		break;

	case cmd_write:
		exmode_cmd_write(data);
		break;

	case cmd_read:
		exmode_cmd_read(data);
		break;

	case cmd_pipe:
		if (data-> mode == MODE_VISUAL)
			exmode_cmd_pipe(data);
		break;

	case cmd_subst:
		exmode_cmd_subst(data);
		break;

	case cmd_substall:
		exmode_cmd_Subst(data);
		break;

	case cmd_swp:
		exmode_cmd_swp(data);
		break;

	case cmd_ieval:
		exmode_cmd_ieval(data);
		break;

	case cmd_reval:
		exmode_cmd_reval(data);
		break;

	case cmd_peval:
		exmode_cmd_peval(data);
		break;

	case cmd_width:
		exmode_cmd_width(data);
		break;

	case cmd_endian:
		data->le ^= 1;
		break;

	/* PREFIX */

	case cmd_prefix:
		return EVH_XPREFIX;

	}

	return EVH_HOLD;
}

struct longcmd {
	const char *name;
	enum command cmd;
};

static void
exmode(struct inputline_data *inpline, void *hookdata)
{
	static const struct longcmd cmds[] = {
		{ "!", cmd_pipe },
		{ "endian", cmd_endian },
		{ "exit", cmd_exit },
		{ "ie", cmd_ieval },
		{ "ieval", cmd_ieval },
		{ "pe", cmd_peval },
		{ "peval", cmd_peval },
		{ "pipe", cmd_pipe },
		{ "q", cmd_quit },
		{ "quit", cmd_quit },
		{ "r", cmd_read },
		{ "read", cmd_read },
		{ "re", cmd_reval },
		{ "reval", cmd_reval },
		{ "s", cmd_subst },
		{ "S", cmd_substall },
		{ "substall", cmd_substall },
		{ "substitute", cmd_subst },
		{ "swp", cmd_swp },
		{ "w", cmd_write },
		{ "width", cmd_width },
		{ "write", cmd_write },
		{ "wq", cmd_exit },
		{ "x", cmd_exit },
		{ NULL }
	};

	struct fileshow_priv *data = hookdata;
	char *cmd = inpline->buf;
	int len = inpline->buf_len;
	const struct longcmd *p;

	/* Trim leading/trailing whitespaces */
	while (isspace(*cmd))
		cmd++, len--;
	while (isspace(cmd[len - 1]) && len > 0)
		len--;

	for (p = cmds; p->name; ++p)
		if (!strcmp(cmd, p->name)) {
			do_command(data, p->cmd);
			return;
		}

	errmsg("Invalid command");
}

struct shortcmd {
	int ch;
	enum command cmd;
};

#define CTRL(ch)	((ch) - 'A' + 1)

static const struct shortcmd commoncmds[] = {
	{ '\e', cmd_normal_mode },
	{ KEY_NPAGE, cmd_page_down },
	{ CTRL('F'), cmd_page_down },
	{ KEY_PPAGE, cmd_page_up },
	{ CTRL('B'), cmd_page_up },
	{ CTRL('E'), cmd_scroll_down },
	{ CTRL('Y'), cmd_scroll_up },
	{ KEY_HOME, cmd_file_begin },
	{ KEY_END, cmd_file_end },
	{ '\t', cmd_switch_column },
	{ KEY_IC, cmd_insert },
	{ KEY_DC, cmd_delchar },
	{ KEY_BACKSPACE, cmd_backspace },
	{ KEY_LEFT, cmd_go_left },
	{ KEY_DOWN, cmd_go_down },
	{ KEY_UP, cmd_go_up },
	{ KEY_RIGHT, cmd_go_right },
	{ KEY_F(2), cmd_write },
	{ KEY_F(10), cmd_exit },
	{ 0 }
};

static const struct shortcmd cmds[] = {
	{ 'G', cmd_file_end },
	{ '0', cmd_line_begin },
	{ '^', cmd_line_begin },
	{ '$', cmd_line_end },
	{ 'h', cmd_go_left },
	{ 'j', cmd_go_down },
	{ 'k', cmd_go_up },
	{ 'l', cmd_go_right },
	{ 'W', cmd_write },
	{ 'Z', cmd_exit },
	{ 'Q', cmd_quit },
	{ ':', cmd_exmode },
	{ '#', cmd_goto },
	{ '/', cmd_search },
	{ '?', cmd_search_back },
	{ 'n', cmd_search_next },
	{ 'N', cmd_search_prev },
	{ 'x', cmd_delchar },
	{ 'X', cmd_delchar_back },
	{ 'v', cmd_visual },
	{ 'y', cmd_clip_yank },
	{ 'd', cmd_clip_delete },
	{ 'p', cmd_clip_paste_after },
	{ 'P', cmd_clip_paste },
	{ 'o', cmd_clip_overwrite },
	{ 'i', cmd_insert },
	{ 'e', cmd_replace },
	{ 'R', cmd_replace },
	{ 'g', cmd_prefix },
	{ 'r', cmd_prefix },
	{ 'm', cmd_prefix },
	{ '\'', cmd_prefix },
	{ '\"', cmd_prefix },
	{ 0 }
};

static const struct shortcmd cmds_g[] = {
	{ 'g', cmd_file_begin },
	{ KEY_ENTER, cmd_goto },
	{ 0 }
};

static enum handler_result
handle_shortcmd(struct fileshow_priv *data, int ch,
		const struct shortcmd *table)
{
	const struct shortcmd *p;

	for (p = table; p->ch; ++p)
		if (ch == p->ch)
			return do_command(data, p->cmd);

	return EVH_PASS;
}

static enum handler_result
fileshow_normal_in(struct fileshow_priv *data, int ch)
{
	enum handler_result ret;

	finish_insert(data);
	ret = handle_shortcmd(data, ch, commoncmds);
	if (ret == EVH_PASS)
		ret = handle_shortcmd(data, ch, cmds);
	return ret;
}

static enum handler_result
fileshow_normal_g_in(struct fileshow_priv *data, int ch)
{
	return handle_shortcmd(data, ch, cmds_g);
}

static void
hexdigit_in(struct fileshow_priv *data, int ch, int *byte)
{
	static const char hx[] = "0123456789abcdef";
	int nibble;

	nibble = strchr(hx, ch) - hx;
	assert(nibble < 16);
	if (data->digitpos == 0) {
		*byte = (data->curbyte & 0xf)  | (nibble << 4);
	} else {
		*byte = (data->curbyte & 0xf0) | nibble;
	}
	data->digitpos++;
}

static enum handler_result
fileshow_normal_r_in(struct fileshow_priv *data, int ch)
{
	enum handler_result res = EVH_PASS;
	int byte = -1;

	if (data->column == FSC_HEX) {
		if (isxdigit(ch)) {
			hexdigit_in(data, tolower(ch), &byte);
			if (data->digitpos > 1) {
				data->digitpos = 0;
				res = EVH_HOLD;
			} else {
				/* Keep @xprefix for the second digit. */
				res = EVH_CUSTOM_XPREFIX;
			}

		} else if (ch == KEY_BACKSPACE && data->digitpos > 0) {
			data->digitpos--;
			res = EVH_CUSTOM_XPREFIX;
		} else if (ch == '\r' || ch == '\033') {
			/* Cancel the replace. */
			data->digitpos = 0;
			res = EVH_HOLD;
		} else {
			/* Persist until the user types something sensible. */
			res = EVH_CUSTOM_XPREFIX;
		}

	} else {
		byte = ch;
		res = EVH_HOLD;
	}

	if (byte >= 0) {
		if (data->mode == MODE_VISUAL) {
			hed_cursor_t base = HED_NULL_CURSOR;
			size_t len = visual_extents(data, &base);
			hed_file_set_bytes(data->file, &base, byte, len);
			hed_put_cursor(&base);
		} else {
			hed_file_set_byte(data->file, &data->cursor, byte);
		}
		data->curbyte = byte;
	}

	return res;
}

static enum handler_result
fileshow_normal_m_in(struct fileshow_priv *data, int ch)
{
	int mark = ch2mark(ch);
	if (mark >= 0)
		hed_dup2_cursor(&data->cursor, &data->marks[mark]);
	else
		errmsg("Invalid mark");
	return EVH_SILENTHOLD;
}

static enum handler_result
fileshow_normal_Qq_in(struct fileshow_priv *data, int ch)
{
	int mark = ch2mark(ch);
	if (mark >= 0) {
		if (mark_is_valid(data, mark))
			set_cursor(data, data->marks[mark].pos);
		else
			errmsg("No such mark");
	} else {
		errmsg("Invalid mark");
	}
	return EVH_HOLD;
}

static enum handler_result
fileshow_normal_QQ_in(struct fileshow_priv *data, int ch)
{
	char reg = ch2reg(ch);
	if (reg >= 0) {
		data->reg = reg;
	} else {
		errmsg("Invalid register");
	}
	return EVH_SILENTHOLD;
}

static enum handler_result
fileshow_byte_in(struct fileshow_priv *data, int ch)
{
	enum handler_result ret = EVH_PASS;
	bool insert;
	int cursmove;
	int byte = -1;

	ret = handle_shortcmd(data, ch, commoncmds);
	if (ret != EVH_PASS)
		return ret;

	insert = (data->mode == MODE_INSERT && data->cursor.pos < UOFF_MAX);

	if (insert && !hed_is_a_cursor(&data->insert)) {
		hed_file_insert_begin(data->file, &data->cursor, &data->insert);
		data->digitpos = 0;
		if (data->offset.pos == data->cursor.pos) {
			hed_dup2_cursor(&data->insert, &data->offset);
			update_idmark(data);
		}
	}

	cursmove = 0;
	if (data->column == FSC_HEX) {
		if (isxdigit(ch)) {
			if (insert && data->digitpos == 0)
				data->curbyte = 0;
			hexdigit_in(data, tolower(ch), &byte);
			if (data->digitpos > 1) {
				data->digitpos = 0;
				cursmove = 1;
			}
			ret = EVH_HOLD;
		}
	} else {
		cursmove = 1;
		byte = ch;
		ret = EVH_HOLD;
	}

	if (byte < 0)
		return ret;

	if (insert) {
		if (data->column != FSC_HEX) {
			hed_file_insert_byte(data->file, &data->insert, byte);
		} else if (data->digitpos > 0) {
			hed_dup2_cursor(&data->insert, &data->cursor);
			hed_file_insert_byte(data->file, &data->insert, byte);
		} else {
			hed_file_set_byte(data->file, &data->cursor, byte);
			hed_move_relative(&data->cursor, 1);
		}
		data->curbyte = byte;

		if (data->cursor.pos - data->ssize >= data->offset.pos)
			slide_viewport(data, data->width);
		else
			assert(data->cursor.pos >= data->offset.pos);
	} else {
		hed_file_set_byte(data->file, &data->cursor, byte);
		data->curbyte = byte;

		jump_relative(data, cursmove);
	}

	return ret;
}

static enum handler_result
fileshow_handler(struct ui_component *comp, const struct ui_event *event)
{
	struct fileshow_priv *data = comp->data;
	enum handler_result res = EVH_PASS;

	switch (event->type) {
		case EV_REDRAW:
			fileshow_redraw(comp);
			break;
		case EV_KEYBOARD:
		{
			int ch = event->v.keyboard.ch;

			if (data->mode == MODE_NORMAL || data->mode == MODE_VISUAL) {
				if (xprefix == 'g') {
					res = fileshow_normal_g_in(data, ch);
				} else if (xprefix == 'r') {
					res = fileshow_normal_r_in(data, ch);
				} else if (xprefix == 'm') {
					res = fileshow_normal_m_in(data, ch);
				} else if (xprefix == '\'') {
					res = fileshow_normal_Qq_in(data, ch);
				} else if (xprefix == '"') {
					res = fileshow_normal_QQ_in(data, ch);
				} else {
					res = fileshow_normal_in(data, ch);
				}
			} else
				res = fileshow_byte_in(data, ch);

			if (res != EVH_PASS && res != EVH_SILENTHOLD)
				fileshow_redraw(comp);
			break;
		}
		case EV_DONE:
			fileshow_done(comp);
			break;
		default:
			fprintf(stderr, "Bug: fileshow_handler E.T. %d\n", event->type);
			break;
	}

	return res;
}
