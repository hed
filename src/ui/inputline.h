/* An input line (exmode, search line, ...) UI component */
/* $Id$ */

/*
 * hed - Hexadecimal editor
 * Copyright (C) 2004  Petr Baudis <pasky@ucw.cz>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of version 2 of the GNU General Public License as
 * published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef HED__UI_INPUTLINE_H
#define HED__UI_INPUTLINE_H

struct inputline_data {
	char *buf;
	int buf_len;
};

/* Note that the struct inputline_data's lifetime may be limited to the
 * handler's execution lifespan. */
typedef void (*inputline_hook)(struct inputline_data *inpline, void *hookdata);

/* Initialize the component. */
void inputline_init(const char *prefix, inputline_hook enter_hook, void *enter_hook_data);

#endif
