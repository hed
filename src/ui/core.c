/* $Id$ */

/*
 * hed - Hexadecimal editor
 * Copyright (C) 2004  Petr Baudis <pasky@ucw.cz>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of version 2 of the GNU General Public License as
 * published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * The Road goes ever on and on
 * Down from the door where it began.
 * Now far ahead the Road has gone,
 * And I must follow, if I can,
 * Pursuing it with weary feet,
 * Until it joins some larger way,
 * Where many paths and errands meet.
 * And whither then? I cannot say.
 */

#include <config.h>

#include <curses.h>
#include <stdlib.h>

#include <hed.h>

#include <term/term.h>
#include <ui/core.h>
#include <ui/error.h>
#include <util/lists.h>


static LIST_HEAD(components);
int xprefix;
int cursor_x, cursor_y;

/* Initialize the UI core. */
void
ui_init(void)
{
	xprefix = '\0';
	cursor_x = cursor_y = 0;
}

/* Kill the UI core. */
void
ui_done(void)
{
	static const struct ui_event event = {
		.type = EV_DONE,
	};

	ui_propevent(&event);
}


/* Components */

/* Register UI component. */
struct ui_component *
ui_register(ui_comp_handler handler, void *data)
{
	struct ui_component *component = calloc(1, sizeof(struct ui_component));

	if (!component) return NULL;
	component->handler = handler;
	component->data = data;
	list_add_tail(&component->list, &components);
	return component;
}

/* Unregister and free UI component. */
void
ui_unregister(struct ui_component *component)
{
	list_del(&component->list);
	free(component);
}


/* Events */

/* Poll for an event. Returns the given event or NULL if nothing happenned.
 * (That may occur when interrupted by a signal or so. (?)) */
int
ui_pollevent(struct ui_event *event)
{
	int ch;

	if (errmsg_queued) {
		errmsg_show(errmsg_queued);
		errmsg_queued = NULL;
	}

	term_goto_cursor(cursor_x, cursor_y);

	/* We poll just for keyboard input now. */
	ch = term_get_char();
	if (ch == ERR)
		return -1;

	event->type = EV_KEYBOARD;
	event->v.keyboard.ch = ch;
	return 0;
}

/* Propagate an event. (Just sending it to all components now. TODO:
 * Hierarchy.) */
void
ui_propevent(const struct ui_event *event)
{
	struct ui_component *component, *ncomponent;
	enum handler_result res;

	if (event->type == EV_REDRAW) {
		list_for_each_entry_safe (component, ncomponent,
					  &components, list) {
			if (!component->handler)
				continue;
			res = component->handler(component, event);
			if (res != EVH_PASS)
				break;
		}
	} else {
		list_for_each_entry_safe_reverse (component, ncomponent,
						  &components, list) {
			if (!component->handler)
				continue;
			res = component->handler(component, event);
			if (res != EVH_PASS)
				break;
		}
	}
	if (res != EVH_CUSTOM_XPREFIX)
		xprefix = '\0';
	if (res == EVH_XPREFIX) {
		assert(event->type == EV_KEYBOARD);
		xprefix = event->v.keyboard.ch;
	}
}
