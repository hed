/* The user interface core */
/* $Id$ */

/*
 * hed - Hexadecimal editor
 * Copyright (C) 2004  Petr Baudis <pasky@ucw.cz>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of version 2 of the GNU General Public License as
 * published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef HED__UI_CORE_H
#define HED__UI_CORE_H

#include "util/lists.h"


/* Initialize the UI core. */
void ui_init(void);

/* Kill the UI core. */
void ui_done(void);

/* Possible prefix */
extern int xprefix; /* Letter prefix (e.g. 'g'). */

/* Final cursor position */
extern int cursor_x, cursor_y;


/* Components */

enum handler_result {
	EVH_PASS,
	EVH_HOLD,
	EVH_SILENTHOLD, // EVH_HOLD, but don't redraw
	EVH_XPREFIX,
	EVH_CUSTOM_XPREFIX, // @xprefix is not cleared after the handler is done
};

/* Gets called to handle an event. */
struct ui_component;
struct ui_event;
typedef enum handler_result (*ui_comp_handler)(struct ui_component *, const struct ui_event *);

struct ui_component {
	struct list_head list;

	ui_comp_handler handler;
	void *data;
};

/* Register UI component. */
struct ui_component *ui_register(ui_comp_handler handler, void *data);

/* Unregister and free UI component. */
void ui_unregister(struct ui_component *component);


/* Events */

struct ui_event {
	enum ui_event_type {
		EV_REDRAW,
		EV_KEYBOARD,
		EV_DONE,
	} type;
	union {
		struct {
			int ch;
		} keyboard;
	} v;
};

/* Poll for an event. Returns negative if nothing happenned.
 * (That may occur e.g. when standard input reaches EOF.) */
int ui_pollevent(struct ui_event *event);

/* Propagate an event. (Just sending it to all components now. TODO:
 * Hierarchy.) */
void ui_propevent(const struct ui_event *event);

/* Trigger a redraw event. */
static void ui_redraw(void);



/**** Inline functions and such. */

static inline void
ui_redraw(void)
{
	static const struct ui_event rev = {
		.type = EV_REDRAW,
	};
	ui_propevent(&rev);
}

#endif
