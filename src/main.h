/* The core */
/* $Id$ */

/*
 * hed - Hexadecimal editor
 * Copyright (C) 2004  Petr Baudis <pasky@ucw.cz>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of version 2 of the GNU General Public License as
 * published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef HED__MAIN_H
#define HED__MAIN_H

/* Set to true if we should head to the end of existence. */
extern bool terminus; /* Ladvi; TODO: Letnany */

/* Return value of the whole program, when terminus will be set. EX_OK by
 * default. */
extern int prg_retval;

#include <sysexits.h>
#define sched_exit(retval) do { prg_retval = retval; terminus = true; } while (0)

/* Commandline options */
extern bool opt_spreadview;

#endif
