/* The main drawing interface */
/* $Id$ */

/*
 * hed - Hexadecimal editor
 * Copyright (C) 2004  Petr Baudis <pasky@ucw.cz>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of version 2 of the GNU General Public License as
 * published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef HED__TERM_TERM_H
#define HED__TERM_TERM_H

/* Initialize the terminal. */
void term_init(void);

/* Deinitialize the terminal. */
void term_done(void);

/* Redraw the terminal. */
void term_redraw(void);


/* Color type; it's an equivalence relation, mathematically. */
typedef unsigned term_color;

#include <curses.h>

/* Color 0 is the default color and cannot be changed */
#define COLOR_NEUTRAL	1	/* background */
#define COLOR_OFFSET	2	/* file offset */
#define COLOR_STATUS	3	/* status line */
#define COLOR_MODE	4	/* editor mode */
#define COLOR_MARK	5	/* Insertion/deletion marks */
#define COLOR_ERROR	6	/* error messages */
#define COLOR_DIRTY	7	/* modified bytes */

/* Keep this a bitmap:
 *   bit 0      0 .. inside file
 *              1 .. outside file
 *   bit 1      0 .. hex
 *              1 .. ASCII
 *   bit 2      0 .. normal
 *              1 .. under cursor
 */

#define COLOR_DATA_OUTSIDE	1
#define COLOR_DATA_ASCII	2
#define COLOR_DATA_CURSOR	4

#define COLOR_DATA_BASE		8
#define COLOR_HEX	(COLOR_DATA_BASE)
#define COLOR_UNHEX	(COLOR_DATA_BASE | COLOR_DATA_OUTSIDE)
#define COLOR_ASC	(COLOR_DATA_BASE | COLOR_DATA_ASCII)
#define COLOR_UNASC	(COLOR_DATA_BASE | COLOR_DATA_ASCII | COLOR_DATA_OUTSIDE)
#define COLOR_CURHEX	(COLOR_DATA_BASE | COLOR_DATA_CURSOR)
#define COLOR_CURUNHEX	(COLOR_DATA_BASE | COLOR_DATA_CURSOR | COLOR_DATA_OUTSIDE)
#define COLOR_CURASC	(COLOR_DATA_BASE | COLOR_DATA_CURSOR | COLOR_DATA_ASCII)
#define COLOR_CURUNASC	(COLOR_DATA_BASE | COLOR_DATA_CURSOR | COLOR_DATA_ASCII | COLOR_DATA_OUTSIDE)

#define COLOR_VISUAL	A_REVERSE	/* Visual blocks */

/* Set the given color to be used for further drawing. */
static void term_set_color(term_color color);

/* Set the given color to be used as the background color. */
static void term_set_bg_color(term_color color);

/* Remove any background color settings */
static void term_unset_bg_color(void);

/* Move the cursor around to the given position. */
void term_goto_cursor(int x, int y);

/* Get window dimensions. */
int term_get_max_x(void);
int term_get_max_y(void);

/* Clear the given line. Can move cursor around. */
void term_clear_line(int y);

/* Put string on the screen at the given position with the given
 * attributes; restores the original color, not cursor position
 * afterwards. */
static void term_print_string(int x, int y, term_color color, const char *str);

/* Print formatted output to the terminal. */
int term_printf(int x, int y, term_color color, const char *fmt, ...);

/* Put char on the screen at the given position with the given
 * attributes; restores the original color, not cursor position. */
static void term_print_char(int x, int y, term_color color, const char chr);


/* Get a character from the terminal. */
int term_get_char(void);



/**** Inline functions and such. */

extern int color2attr[64];

static inline void
term_set_color(term_color color)
{
	attrset(color2attr[color & ~A_REVERSE] | (color & A_REVERSE));
}

static inline void
term_set_bg_color(term_color color)
{
	bkgdset(color2attr[color & ~A_REVERSE] | (color & A_REVERSE));
}

static inline void
term_unset_bg_color(void)
{
	bkgdset(0);
}

static inline void
term_print_string(int x, int y, term_color color, const char *str)
{
	term_set_color(color);
	mvaddstr(y, x, str);
}

static inline void
term_print_char(int x, int y, term_color color, const char chr)
{
	term_set_color(color);
	mvaddch(y, x, chr);
}

#endif
