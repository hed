/* $Id$ */

/*
 * hed - Hexadecimal editor
 * Copyright (C) 2004  Petr Baudis <pasky@ucw.cz>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of version 2 of the GNU General Public License as
 * published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * 'In any case we must be called at dawn,' said Frodo. 'We must get off as
 * early as possible. Breakfast at six-thirty, please.'
 *
 * 'Right! I'll see to the orders,' said the landlord. 'Good night, Mr.
 * Baggins - Underhill, I should say! Good night -'
 */

#include <config.h>

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <hed.h>

#include <config/config.h>
#include <util/lists.h>


/* This is really just trivial implementation. */

struct option {
	struct list_head list;
	char *name;
	char *value;
};
static LIST_HEAD(options);

static void
parse_config(char *cfgfile)
{
	char line[1024]; // no lines longer than that
	FILE *cfg = fopen(cfgfile, "r");
	if (!cfg) return;

#define skip_spaces \
	do { \
		while (isspace(*l)) \
			l++; \
	} while (0)
#define syntax_error \
	{ \
		fprintf(stderr, "Warning: %s: Syntax error at '%s'\n", cfgfile, l); \
		continue; \
	}

	while (fgets(line, 1024, cfg)) {
		char *l;
		char *name;
		char *value;

		if (line[strlen(line)-1] != '\n') {
			fprintf(stderr, "Warning: %s: too long line or no newline at the end of file; aborting parsing\n", cfgfile);
			goto fail;
		}
		line[strlen(line)-1] = 0;

		l = line;
		skip_spaces;
		if (!*l)
			continue;
		if (strncmp(l, "set ", 4))
			syntax_error;
		l += 4;
		skip_spaces;
		name = l;
		while (!isspace(*l) && *l)
			l++;
		if (!*l) {
auto_true:
			value = "1";
			goto fill;
		}
		*l++ = 0;
		skip_spaces;
		if (!*l)
			goto auto_true;
		value = l;
		l += strlen(l) - 1;
		while (isspace(*l) && l > value)
			l--;
		if (l > value)
			*++l = 0;
fill:
		{
			struct option *opt = calloc(1, sizeof(struct option));
			opt->name = strdup(name);
			opt->value = strdup(value);
			list_add(&opt->list, &options);
		}
	}

fail:
	fclose(cfg);
}

void
config_init(void)
{
	char *home = getenv("HOME");
	parse_config("/etc/hedrc");
	if (home) {
		char *x = malloc(strlen(home) + 7 + 1);
		sprintf(x, "%s/.hedrc", home);
		parse_config(x);
	}
}

static struct option *
get_opt(char *name)
{
	struct option *opt;
	list_for_each_entry (opt, &options, list) {
		if (!strcmp(name, opt->name))
			return opt;
	}
	return NULL;
}

char *
get_opt_str(char *name, char *defaultval)
{
	struct option *opt = get_opt(name);
	return opt ? opt->value : defaultval;
}

int
get_opt_int(char *name, int defaultval)
{
	struct option *opt = get_opt(name);
	return opt ? atoi(opt->value) : defaultval;
}

bool
get_opt_bool(char *name, bool defaultval)
{
	struct option *opt = get_opt(name);
	bool val = defaultval;
	if (opt) {
		if (!strcmp(opt->value, "0") ||
		    !strcmp(opt->value, "false"))
			val = false;
		else
			val = true;
	}
	return val;
}
