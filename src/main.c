/* $Id$ */

/*
 * hed - Hexadecimal editor
 * Copyright (C) 2004  Petr Baudis <pasky@ucw.cz>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of version 2 of the GNU General Public License as
 * published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * 'Away now, Shadowfax! Run, greatheart, run as you have never run before! Now
 * we are come to the lands where you were foaled and every stone you know. Run
 * now! Hope is in speed!'
 *
 * Shadowfax tossed his head and cried aloud, as if a trumpet had summoned him
 * to battle. Then he sprang forward. Fire flew from his feet; night rushed
 * over him.
 *
 * As he fell slowly into sleep, Pippin had a strange feeling: he and Gandalf
 * were still as stone, seated upon the statue of a running horse, while the
 * world rolled away beneath his feet with a great noise of wind.
 */

/* Feature macros needed for:
 *  - getopt_long
 */
#define _GNU_SOURCE

#include <config.h>

#include <errno.h>
#include <getopt.h>
#include <stdlib.h>
#include <sysexits.h> /* XXX: Portability? */

#include <hed.h>

#include <config/config.h>
#include <libhed/file.h>
#include <term/term.h>
/* #include <ui/cmdline.h> */
#include <ui/core.h>
#include <ui/fileshow.h>
/* #include <ui/windowbar.h> */
#include <main.h>

#define VERSION "0.5"


static char *kbdplay;

bool terminus = 0;
int prg_retval = EX_OK;

bool opt_spreadview = 0;

static const char namevers[] = "HEd " VERSION;
static const char maintainer[] = "<pasky@ucw.cz>";

#ifdef HAVE_GETOPT_LONG
# define LONG_OR_SHORT(l, s) l
#else
# define LONG_OR_SHORT(l, s) s
#endif

static void
complain_swap(struct hed_file *file)
{
	fprintf(stderr,
		"Error: Swap file %s for file %s exists\n"
		"This means that hed did not quit properly the last time\n"
		"you edited this file. You can use the swap file to restore\n"
		"some of your unsaved work. Run hed with --restore-swp to\n"
		"do that (until you save your file, the swap file will\n"
		"stay around). Run hed with --remove-swp to get rid of\n"
		"the file. (You can also use both options simultanously.)\n",
		hed_file_swap_name(file), file->name);
}

static void
usage(char *exe)
{
	printf("%s, a visual hexadecimal editor.\n"
	       "Usage: %s [OPTION]... FILE\n"
	       "\n"
	       "Startup:\n"
    LONG_OR_SHORT(
		  "  -V, --version   display the version of HEd and exit.\n",
		  "  -V      display the version of HEd and exit.\n")
    LONG_OR_SHORT(
		  "  -h, --help      print this help.\n",
		  "  -h      print this help.\n")
	       "\n"
	       "Visuals:\n"
    LONG_OR_SHORT(
		  "  -s, --spread    fill the maximal width of line.\n",
		  "  -s      fill the maximal width of line.\n")
	       "\n"
	       "Controls:\n"
    LONG_OR_SHORT(
		  "      --normal    start in the N(ormal) mode (DEFAULT).\n",
		  "")
    LONG_OR_SHORT(
		  "  -r, --replace   start in the R(eplace) mode.\n",
		  "  -r      start in the R(eplace) mode.\n")
    LONG_OR_SHORT(
		  "  -i, --insert    start in the I(nsert) mode.\n",
		  "  -i      start in the I(nsert) mode.\n")
	       "\n"
    LONG_OR_SHORT(
		  "  -p, --play=...  the value is interpreted as keyboard input fed to hed.\n",
		  "  -p ...  the value is interpreted as keyboard input fed to hed.\n")
	       "\n"
	       "Mail bug reports and suggestions to %s.\n",
	       namevers, exe, maintainer);
	exit(EX_USAGE);
}

static void
version(void)
{
	printf("%s\n\n"
	       "\n"
	       "Copyright (C) 2004, 2005  Petr Baudis <pasky@ucw.cz>\n"
	       "This program is distributed in the hope that it will be useful,\n"
	       "but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
	       "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n"
	       "GNU General Public License for more details.\n",
	       namevers);
	exit(EX_USAGE);
}

int
main(int argc, char *argv[])
{
	struct hed_file *file;
	bool show_usage = 0, show_version = 0;
	bool remove_swp = 0, restore_swp = 0;

	while (1) {
#ifdef HAVE_GETOPT_LONG
		static struct option long_options[] = {
			{ "help",       0, 0, 'h' },
			{ "insert",     0, 0, 'i' },
			{ "normal",     0, 0, 1 },
			{ "play",       1, 0, 'p' },
			{ "remove-swp", 0, 0, 2 },
			{ "replace",    0, 0, 'r' },
			{ "restore-swp",0, 0, 3 },
			{ "spread",     0, 0, 's' },
			{ "version",    0, 0, 'V' },
			{ NULL,         0, 0, 0 }
		};
		int optindex;
#endif
		//char *numguard = NULL;
		int c;

#ifdef HAVE_GETOPT_LONG
		c = getopt_long(argc, argv, "hiprsV", long_options, &optindex);
#else
		c = getopt(argc, argv, "hiprsV");
#endif
		if (c == -1) break;

		switch (c) {
			case 'h':
			case '?': show_usage = 1; break;
			case 'i': opt_defmode = MODE_INSERT; break;
			case 1  : opt_defmode = MODE_NORMAL; break;
			case 'r': opt_defmode = MODE_REPLACE; break;
			case 's': opt_spreadview = 1; break;
			case 'V': show_version = 1; break;
			case 2  : remove_swp = 1; break;
			case 3  : restore_swp = 1; break;
			case 'p': kbdplay = optarg; break;
		}
	}

	if (show_version) version();
	if (show_usage || argc <= optind) usage(argv[0]);

	config_init();

	/* Do not check for errors. In the worst case the editor won't
	 * handle file truncates, but that's usually not fatal.
	 * TODO: Issue a warning in such a way that it doesn't get
	 * immediately overwritten.
	 */
	libhed_init();

	file = hed_open(argv[optind]);
	if (!file) {
		perror(argv[optind]);
		exit(EX_NOINPUT);
	}

	if (restore_swp) {
		if (hed_file_read_swap(file)) {
			if (errno)
				perror(file->name);
			else
				fprintf(stderr, "Restoring %s failed, sorry\n",
					file->name);
			hed_close(file);
			exit(EX_NOINPUT);
		}
		if (remove_swp)
			hed_file_remove_swap(file);

	} else {
		if (remove_swp)
			hed_file_remove_swap(file);
		if (hed_file_has_swap(file)) {
			complain_swap(file);
			hed_close(file);
			exit(EX_NOINPUT);
		}
	}

	term_init();
	ui_init();
	/* windowbar_init(); */
	fileshow_init(file);
	/* cmdline_init(); */

	ui_redraw();

	while (!terminus) {
		struct ui_event event;

		if (kbdplay && *kbdplay) {
			event.type = EV_KEYBOARD;
			event.v.keyboard.ch = *kbdplay++;
			ui_propevent(&event);
		} else {
			if (ui_pollevent(&event))
				break;
			ui_propevent(&event);
		}
	}

	ui_done();
	term_done();
	hed_close(file);

	exit(prg_retval);
}
