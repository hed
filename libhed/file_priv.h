/* This is a libhed PRIVATE file.
 * Do not include outside of libhed. Do not install on the system.
 */

#ifndef LIBHED__FILE_PRIV_H
#define LIBHED__FILE_PRIV_H

#include <sys/stat.h>

#include "types.h"
#include "file.h"
#include "tree.h"

struct file_priv {
	/* Public fields */
	struct hed_file f;

	/* The file blocks are sorted by the offset in this list. */
	struct hed_tree blocks;

	/* This list contains clean file-backed blocks.
	 * It is sorted from least recently used to most recently used. */
	struct hed_list_head lru;

	/* This cache holds the data for clean file-backed blocks. */
	struct hed_cache *cache;

	int fd;			/* file descriptor for read-only access */
	struct stat s;

	/* the always-present terminal block */
	struct hed_block terminal_block;

#ifdef HED_CONFIG_READAHEAD
	enum hed_readahead readahead;
#endif

#ifdef HED_CONFIG_SWAP
	char *swpname;		/* original swap file name */
	char *newswpname;	/* new swap file name */
	struct swp_file *swp;
#endif

#ifdef HED_CONFIG_MMAP
	struct hed_list_head fixup_list;
#endif
};

#define file_private(file) ({			\
        const struct hed_file *__f = (file);	\
        (struct file_priv *)( (char *)__f - offsetof(struct file_priv,f) );})

#endif	/* LIBHED__FILE_PRIV_H */
