/* The expression evaluator */

/* This is a libhed PUBLIC file.
 * This header file will be installed on the target system.
 * When you add new things here, make sure they start with hed_.
 */

/*
 * hed - Hexadecimal editor
 * Copyright (C) 2004  Petr Baudis <pasky@ucw.cz>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of version 2 of the GNU General Public License as
 * published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef LIBHED__EXPR_H
#define LIBHED__EXPR_H

#include <libhed/types.h>

/*******************************************************************
 * BYTE STRINGS
 */

hed_uoff_t hed_bytestr2off(unsigned char *bytestr, size_t len, long flags);
void hed_off2bytestr(unsigned char *bytestr, size_t len, hed_uoff_t num);

/*******************************************************************
 * EXPRESSIONS
 */

typedef long (*hed_expr_reg_cb)(void *data, char reg, hed_uoff_t ofs, /*  */
				unsigned char *scramble, size_t len);
typedef long (*hed_expr_mark_cb)(void *data, char mark,
				 unsigned char *scramble, size_t len);

struct hed_expr {
	long flags;
	unsigned char *buf;
	size_t len;
};

/* Return flags for callbacks */
#define HED_AEF_NEGATIVE 1	// the result should be treated as negative
#define HED_AEF_SIGNED   2	// take the sign bit from the result
#define HED_AEF_DYNAMIC  4	// result is not constant

#define HED_AEF_ENDIANMASK	(3<<3)	// endianity mask
#define HED_AEF_KEEPENDIAN	(0<<3)	// keep endianity (no change)
#define HED_AEF_FORCELE		(1<<3)	// force little-endian
#define HED_AEF_FORCEBE		(2<<3)	// force big-endian
#define HED_AEF_SWAPENDIAN	(3<<3)	// always swap endianity

#define HED_AEF_ERROR	(~(~0UL>>1))

/* NB: The HED_AEF_NEGATIVE flag behaves as an extra "hidden" MSB bit,
 * which is used in calculations, but not included in the final result.
 * This allows to fit unsigned values in the range 0..2^n-1 as well as
 * signed values from -2^(n-1)..2^(n-1)-1 into only n bits.
 *
 * In particular, a zero with HED_AEF_NEGATIVE set is not treated as
 * a negative zero, but rather as the largest negative signed value,
 * e.g. "00" byte string with HED_AEF_NEGATIVE represents -256.
 *
 * HED_AEF_NEGATIVE takes precedence over HED_AEF_SIGNED. The following
 * table shows the relation betwen the various bits and the resulting sign:
 *
 *   HED_AEF_NEGATIVE  HED_AEF_SIGNED  MSB  result
 *      0                 0             x   positive
 *      0                 1             0   positive
 *      0                 1             1   negative
 *      1                 x             x   negative
 */

struct hed_expr *hed_expr_new(char *sexpr,
			      hed_expr_reg_cb reg_cb,
			      hed_expr_mark_cb mark_cb,
			      void *cb_data);
long hed_expr_eval(struct hed_expr *expr);
void hed_expr_free(struct hed_expr *expr);

static inline size_t
hed_expr_len(const struct hed_expr *expr)
{
	return expr->len;
}

static inline unsigned char *
hed_expr_buf(const struct hed_expr *expr)
{
	return expr->buf;
}

static inline long
hed_expr_flags(const struct hed_expr *expr)
{
	return expr->flags;
}

static inline hed_uoff_t
hed_expr2off(const struct hed_expr *expr)
{
	return hed_bytestr2off(hed_expr_buf(expr), hed_expr_len(expr),
			       hed_expr_flags(expr));
}

#endif
