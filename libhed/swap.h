/* Swap file handling */

/* This is a libhed PRIVATE file.
 * Do not include outside of libhed. Do not install on the system.
 */

/*
 * hed - Hexadecimal editor
 * Copyright (C) 2011  Petr Tesarik <petr@tesarici.cz>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of version 2 of the GNU General Public License as
 * published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef LIBHED__SWAP_H
#define LIBHED__SWAP_H

#include "config.h"
#include "types.h"

/* Prevent polluting linker namespace with internal symbols */
#include "private.h"
#define swp_cpin internal_name(swp_cpin)
#define swp_done internal_name(swp_done)
#define swp_free internal_name(swp_free)
#define swp_init_read internal_name(swp_init_read)
#define swp_init_write internal_name(swp_init_write)
#define swp_malloc internal_name(swp_malloc)
#define swp_private internal_name(swp_private)
#define swp_shrink internal_name(swp_shrink)
#define swp_write internal_name(swp_write)
#define swp_zalloc internal_name(swp_zalloc)

#ifdef HED_CONFIG_SWAP

#define SWAP_SIGNATURE	"HEDSWAP\n"
#define SIG_LEN		(sizeof(SWAP_SIGNATURE) - 1)
#define SWAP_VERSION	2

struct swp_header {
	char signature[SIG_LEN]; /* = "HEDSWAP\n" */
	short version;		 /* Swap file version */
	size_t offsize;
	void *addr;		 /* Address of the file header itself */
};

struct swp_alloc;

struct swp_file {
	struct swp_header *hdr;

	/* Swap implementation private fields */
	int fd;
	hed_uoff_t size;

#ifdef HED_CONFIG_SWAP_MMAP
	int mmap_prot;
	int mmap_flags;
#endif

	struct list_head freeblocks;
	struct list_head usedblocks;
	void ***maps;
	unsigned nmaps;
};

struct swp_file *swp_init_write(const char *name);
struct swp_file *swp_init_read(const char *name);
int swp_done(struct swp_file *swp);
void *swp_malloc(struct swp_file *swp, size_t size);
void *swp_zalloc(struct swp_file *swp, size_t size);
void *swp_shrink(struct swp_file *swp, void *ptr, size_t newsize);
void swp_free(struct swp_file *swp, void *ptr);

int swp_write(struct swp_file *swp);
size_t swp_cpin(struct swp_file *swp, void *buf, void *ptr, size_t len);

static inline void *
swp_private(struct swp_file *swp)
{
	return swp->hdr + 1;
}

#else  /* HED_CONFIG_SWAP */

#include <stdlib.h>

struct swp_file;

static inline void *
swp_malloc(struct swp_file *swp, size_t size)
{
	return malloc(size);
}

static inline void *
swp_zalloc(struct swp_file *swp, size_t size)
{
	return calloc(1, size);
}

static inline void *
swp_shrink(struct swp_file *swp, void *ptr, size_t newsize)
{
	return realloc(ptr, newsize);
}

static inline void
swp_free(struct swp_file *swp, void *ptr)
{
	free(ptr);
}

#endif /* HED_CONFIG_SWAP */

#endif	/* LIBHED__SWAP_H */
