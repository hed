/* File cache handling */

/* This is a libhed PRIVATE file.
 * Do not include outside of libhed. Do not install on the system.
 */

/*
 * hed - Hexadecimal editor
 * Copyright (C) 2008  Petr Tesarik <petr@tesarici.cz>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of version 2 of the GNU General Public License as
 * published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef LIBHED__CACHE_H
#define LIBHED__CACHE_H

#include <util/lists.h>
#include "types.h"
#include "swap.h"

/* Prevent polluting linker namespace with internal symbols */
#include "private.h"
#define cache_alloc internal_name(cache_alloc)
#define cache_compact internal_name(cache_compact)
#define cache_done internal_name(cache_done)
#define cache_get internal_name(cache_get)
#define cache_init internal_name(cache_init)
#define cache_put internal_name(cache_put)
#define _block_data_free internal_name(_block_data_free)
#define block_data_new internal_name(block_data_new)
#define block_data_shrink internal_name(block_data_shrink)

struct hed_cache {
	struct list_head free;
	unsigned nelem, nfree;
	struct hed_block_data **revmap;
	struct swp_file *swp;
	void *data;
};

/* Initialize a cache object with @nelem entries.
 */
struct hed_cache *cache_init(unsigned nelem, struct swp_file *swp);

/* Finalize a cache object */
void cache_done(struct hed_cache *cache);

/* Allocate an object from the cache.
 * The object is automatically referenced, so the caller must
 * call cache_put() on it when it is no longer needed.
 * Returns NULL if all slots are full.
 */
struct hed_block_data *cache_alloc(struct hed_cache *cache);

/* Compact an array of data items so that their data lies
 * in a contiguous memory chunk. */
void cache_compact(struct hed_cache *cache,
			struct hed_block_data **items, int n);

/* Allocate a new object outside of the cache.
 * The object can hold @size bytes of data and its reference count
 * will be exactly one.
 * Returns NULL on allocation failure. */
struct hed_block_data *block_data_new(struct hed_cache *cache, size_t size);

/* Shrink an object to a new data size.
 * The new size must be less or equal to the old size.
 * NB: This can be done only to out-of-cache blocks.
 */
void block_data_shrink(struct hed_cache *cache,
		       struct hed_block_data *data, size_t newsize);

/* Free a data object.
 * This method is called internally by cache_put() when the
 * reference count drops to zero. It should never be called directly.
 */
void _block_data_free(struct hed_cache *cache, struct hed_block_data *data);

/* Get a reference to a cache object. */
static inline void
cache_get(struct hed_block_data *data)
{
	++data->u2.used.refcount;
}

/* Release a reference to a cache object. */
static inline void
cache_put(struct hed_cache *cache, struct hed_block_data *data)
{
	if (!--data->u2.used.refcount)
		_block_data_free(cache, data);
}

#endif
