/* $Id$ */

/*
 * hed - Hexadecimal editor
 * Copyright (C) 2004  Petr Baudis <pasky@ucw.cz>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of version 2 of the GNU General Public License as
 * published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * 'You see: Mr. Drogo, he married poor Miss Primula Brandybuck. She was our
 * Mr. Bilbo's first cousin on the mother's side (her mother being the youngest
 * of the Old Took's daughters); and Mr. Drogo was his second cousin. So Mr.
 * Frodo is his first _and_ second cousin, once removed either way, as the
 * saying is, if you follow me.
 */

#include <config.h>

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <endian.h>
#include <assert.h>

#include <util/numbers.h>
#include <util/lists.h>

#include "expr.h"

/* This is really just trivial implementation. */

struct hed_expr_priv {
	struct hed_list_head atoms;
	struct hed_expr e;
	hed_expr_reg_cb reg_cb;
	hed_expr_mark_cb mark_cb;
	void *cb_data;
	size_t cb_pos;
};

#define SIZE_AUTO	((size_t)-1L)

struct atom {
	struct list_head list;

	size_t len;		// SIZE_AUTO == auto (defaults to 1 at root)

	enum atom_type {
		ATOM_UN,
		ATOM_BIN,
		ATOM_BYTESTR,
		ATOM_REGISTER,
		ATOM_MARK,
		ATOM_T_MAX
	} type;
};

/* atom_evaluator returns a flag bitmask, see the HED_AEF_xxx constants */
typedef long atom_evaluator(struct hed_expr_priv *expr, struct atom *atom,
			    unsigned char *scramble, size_t len);
static atom_evaluator atom_un;
static atom_evaluator atom_bin;
static atom_evaluator atom_bytestr;
static atom_evaluator atom_register;
static atom_evaluator atom_mark;

typedef void (*atom_free)(void *atom);

static void expr_cleanup(struct hed_expr_priv *expr);

struct atom_un {
	struct atom a;
	enum atom_un_op {
		AUO_NEG,
		AUO_MINUS,
	} op;
	struct atom *atom;
};

struct atom_bin {
	struct atom a;
	enum atom_bin_op {
		ABO_OR,
		ABO_AND,
		ABO_XOR,
		ABO_PLUS,
		ABO_MINUS,
		ABO_MUL,
		ABO_DIV,
		ABO_MOD,
		ABO_SHL,
		ABO_SHR,
		/* pseudo-operations used while parsing */
		ABO_LPAREN,
		ABO_RPAREN,
	} op;
	struct atom *atom1;
	struct atom *atom2;
};

struct atom_bytestr {
	struct atom a;
	unsigned char bytes[0]; /* [len] */
};

struct atom_register {
	struct atom a;
	char reg;
	struct hed_expr_priv offset;
};

struct atom_mark {
	struct atom a;
	char mark;
};

static unsigned
unhex(char x)
{
	static const char hx[] = "0123456789abcdef89ABCDEF";
	unsigned idx = strchr(hx, x) - hx;
	return (idx & 0x0f) | ((idx & 0x10) >> 1);
}

static inline void
skip_white(char **sexpr)
{
	while (isspace(**sexpr))
		(*sexpr)++;
}

static struct atom *val_parse(char **sexpr);
static struct hed_expr_priv *compile_till(struct hed_expr_priv *expr,
					  char **sexpr, char till);
static long atom_eval(struct hed_expr_priv *expr, struct atom *atom,
		      unsigned char *scramble, size_t len);
static void free_atom(struct atom *atom);

#if __BYTE_ORDER == __LITTLE_ENDIAN
# define BYTESTR_LSB(s, len)	(s)
# define BYTESTR_STEP		(+1)
# define BYTESTR_MSB(s, len)	((s) + (len) - 1)
# define BYTESTR_LOPART(s, len, plen)	(s)
# define BYTESTR_HIPART(s, len, plen)	((s) + (len) - (plen))
# define HED_AEF_NATIVEORDER	HED_AEF_FORCELE
#elif __BYTE_ORDER == __BIG_ENDIAN
# define BYTESTR_LSB(s, len)	((s) + (len) - 1)
# define BYTESTR_STEP		(-1)
# define BYTESTR_MSB(s, len)	(s)
# define BYTESTR_LOPART(s, len, plen)	((s) + (len) - (plen))
# define BYTESTR_HIPART(s, len, plen)	(s)
# define HED_AEF_NATIVEORDER	HED_AEF_FORCEBE
#else
# error "Unsupported byte order"
#endif

hed_uoff_t
hed_bytestr2off(unsigned char *bytestr, size_t len, long flags)
{
	unsigned char *p, *q;
	int step;
	register hed_uoff_t ret;

	if ((flags & HED_AEF_ENDIANMASK) == HED_AEF_KEEPENDIAN ||
	    (flags & HED_AEF_ENDIANMASK) == HED_AEF_NATIVEORDER) {
		p = BYTESTR_MSB(bytestr, len);
		q = BYTESTR_LSB(bytestr, len);
		step = -BYTESTR_STEP;
	} else {
		p = BYTESTR_LSB(bytestr, len);
		q = BYTESTR_MSB(bytestr, len);
		step = BYTESTR_STEP;
	}

	if (flags & HED_AEF_SIGNED && (signed char)*p < 0)
		flags |= HED_AEF_NEGATIVE;
	ret = flags & HED_AEF_NEGATIVE ? -1 : 0;

	q += step;
	while (p != q) {
		ret <<= 8;
		ret += *p;
		p += step;
	}
	return ret;
}

/* The following function always uses the native byte order.
 * FIXME? Should the API be more analogous to hed_bytestr2off?
 */
void
hed_off2bytestr(unsigned char *bytestr, size_t len, hed_uoff_t num)
{
	unsigned char *p = BYTESTR_LSB(bytestr, len);
	unsigned char *q = BYTESTR_MSB(bytestr, len) + BYTESTR_STEP;
	while (p != q) {
		*p = num;
		num >>= 8;
		p += BYTESTR_STEP;
	}
}

/* bytestr arithmetics */
static void
bytestr_not(unsigned char *s, size_t len)
{
	while (len--) {
		*s = ~*s;
		++s;
	}
}

static void
bytestr_and(unsigned char *s1, unsigned char *s2, size_t len)
{
	while (len--)
		*s1++ &= *s2++;
}

static void
bytestr_or(unsigned char *s1, unsigned char *s2, size_t len)
{
	while (len--)
		*s1++ |= *s2++;
}

static void
bytestr_xor(unsigned char *s1, unsigned char *s2, size_t len)
{
	while (len--)
		*s1++ ^= *s2++;
}

/* Get the maximum length of the shift bytestr and check that the
 * actual value in @s fits into that size. If @s is larger than
 * the maximum shift for @len-sized numbers, return 0. This would
 * be otherwise an invalid value, because even a zero-sized bytestr
 * returns 1.
 */
static size_t
max_shiftlen(unsigned char *s, size_t len)
{
	size_t maxshiftoff = ((bitsize(len) + 3) - 1) >> 3;
	unsigned char *p = BYTESTR_LSB(s, len) + maxshiftoff * BYTESTR_STEP;
	while (p != BYTESTR_MSB(s, len)) {
		p += BYTESTR_STEP;
		if (*p)
			return 0;
	}
	return maxshiftoff + 1;
}

/* Get the shift size in bytes. */
static size_t
shift_bytecount(unsigned char *s, size_t len, size_t shiftlen)
{
	unsigned carry = 0;
	size_t ret = 0;
	s = BYTESTR_LSB(s, len) + shiftlen * BYTESTR_STEP;
	while (shiftlen--) {
		s -= BYTESTR_STEP;
		ret = (ret << 8) | carry | (*s >> 3);
		carry = (*s & 7) << 5;
	}
	return ret;
}

static void
bytestr_shl(unsigned char *s1, unsigned char *s2, size_t len)
{
	size_t shiftlen, byteoff, bitshift;

	shiftlen = max_shiftlen(s2, len);
	byteoff = shiftlen ? shift_bytecount(s2, len, shiftlen) : len;
	if (byteoff < len) {
		unsigned char *p;

		bitshift = *BYTESTR_LSB(s2, len) & 7;
		for (p = BYTESTR_MSB(s1, len) - byteoff * BYTESTR_STEP;
		     p != BYTESTR_LSB(s1, len);
		     p -= BYTESTR_STEP)
			p[byteoff * BYTESTR_STEP] =
				*p << bitshift |
				p[-BYTESTR_STEP] >> (8 - bitshift);
		p[byteoff * BYTESTR_STEP] = *p << bitshift;
	} else
		byteoff = len;
	memset(BYTESTR_LOPART(s1, len, byteoff), 0, byteoff);
}

static void
bytestr_shr(unsigned char *s1, unsigned char *s2, size_t len)
{
	size_t shiftlen, byteoff, bitshift;

	shiftlen = max_shiftlen(s2, len);
	byteoff = shiftlen ? shift_bytecount(s2, len, shiftlen) : len;
	if (byteoff < len) {
		unsigned char *p;

		bitshift = *BYTESTR_LSB(s2, len) & 7;
		for (p = BYTESTR_LSB(s1, len) + byteoff * BYTESTR_STEP;
		     p != BYTESTR_MSB(s1, len);
		     p += BYTESTR_STEP)
			p[-byteoff * BYTESTR_STEP] =
				*p >> bitshift |
				p[BYTESTR_STEP] << (8 - bitshift);
		p[-byteoff * BYTESTR_STEP] = *p >> bitshift;
	} else
		byteoff = len;
	memset(BYTESTR_HIPART(s1, len, byteoff), 0, byteoff);
}

static unsigned
bytestr_inc(unsigned char *s, size_t len)
{
	unsigned carry;
	s = BYTESTR_LSB(s, len);
	do {
		carry = !++(*s);
		s += BYTESTR_STEP;
	} while (carry && --len);
	return carry;
}

static unsigned
bytestr_add(unsigned char *s1, unsigned char *s2, size_t len)
{
	unsigned carry = 0;
	s1 = BYTESTR_LSB(s1, len);
	s2 = BYTESTR_LSB(s2, len);
	while (len--) {
		carry += *s1 + *s2;
		*s1 = carry;
		carry >>= 8;
		s1 += BYTESTR_STEP;
		s2 += BYTESTR_STEP;
	}
	return carry;
}

static unsigned
bytestr_sub(unsigned char *s1, unsigned char *s2, size_t len)
{
	signed carry = 0;
	s1 = BYTESTR_LSB(s1, len);
	s2 = BYTESTR_LSB(s2, len);
	while (len--) {
		carry += *s1 - *s2;
		*s1 = carry;
		carry >>= 8;
		s1 += BYTESTR_STEP;
		s2 += BYTESTR_STEP;
	}
	return carry;
}

/* multiply @src by @mul and add it to @dst */
static unsigned
muladd(unsigned char *dst, unsigned char *src, size_t len, unsigned char mul)
{
	unsigned carry = 0;
	unsigned char *p = BYTESTR_LSB(src, len);
	unsigned char *q = BYTESTR_LSB(dst, len);
	while (p != BYTESTR_MSB(src, len) + BYTESTR_STEP) {
		carry += *p * mul + *q;
		*q = carry;
		carry >>= 8;
		p += BYTESTR_STEP;
		q += BYTESTR_STEP;
	}
	return carry;
}

static void
bytestr_mul(unsigned char *s1, unsigned char *s2, size_t len)
{
	unsigned char ret[len];
	size_t mlen;

	memset(ret, 0, len);
	for (mlen = len; mlen; --mlen)
		muladd(BYTESTR_HIPART(ret, len, mlen),
		       BYTESTR_LOPART(s1, len, mlen), mlen,
		       BYTESTR_MSB(s2, len)[(1-mlen) * BYTESTR_STEP]);
	memcpy(s1, ret, len);
}

static unsigned
shl_one(unsigned char *s, size_t len, unsigned carry)
{
	unsigned char *p = BYTESTR_LSB(s, len);
	while (p != BYTESTR_MSB(s, len) + BYTESTR_STEP) {
		carry |= *p << 1;
		*p = carry;
		carry >>= 8;
		p += BYTESTR_STEP;
	}
	return carry;
}

/* Simple non-restoring algorithm for now.
 * Feel free to improve.
 */
static void
bytestr_div(unsigned char *s1, unsigned char *s2, size_t len,
	    unsigned char *mod)
{
	unsigned char *p, mask;

	memset(mod, 0, len);
	p = BYTESTR_MSB(s1, len);
	mask = 0x80;
	while (p != BYTESTR_LSB(s1, len) - BYTESTR_STEP) {
		if (shl_one(mod, len, !!(*p & mask)))
			bytestr_add(mod, s2, len);
		else
			bytestr_sub(mod, s2, len);

		if (*BYTESTR_MSB(mod, len) & 0x80)
			*p &= ~mask;
		else
			*p |= mask;

		if (! (mask >>= 1) ) {
			p -= BYTESTR_STEP;
			mask = 0x80;
		}
	}
	if (*BYTESTR_MSB(mod, len) & 0x80)
		bytestr_add(mod, s2, len);
}

/* Remove any MSB zeroes from @s and update *@plen. */
static unsigned char*
bytestr_shrink(unsigned char *s, size_t *plen)
{
	size_t origlen = *plen;
	unsigned char *p = BYTESTR_MSB(s, origlen);

	while (*plen > 1 && !*p) {
		--(*plen);
		p -= BYTESTR_STEP;
	}
	return memmove(s, BYTESTR_LOPART(s, origlen, *plen), *plen);
}

static long
bytestr_unop(unsigned char *s, size_t len, long flags, enum atom_un_op op)
{
	switch (op) {
		case AUO_NEG:
		case AUO_MINUS:
			bytestr_not(s, len);
			if (op == AUO_MINUS) {
				if (!bytestr_inc(s, len))
					flags ^= HED_AEF_NEGATIVE;
			}
			break;
		default:
			assert(0);
			break;
	}
	return flags;
}

static long
bytestr_binop(unsigned char *s1, unsigned char *s2, size_t len,
	      long flags1, long flags2, enum atom_bin_op op)
{
	unsigned char *smod;
	unsigned carry;
	int sign  = !!(flags1 & HED_AEF_NEGATIVE);
 	int sign2 = !!(flags2 & HED_AEF_NEGATIVE);
	long ret = (flags1 & ~HED_AEF_NEGATIVE) | (flags2 & HED_AEF_DYNAMIC);

	switch (op) {
		case ABO_OR:
			sign |= sign2;
			bytestr_or(s1, s2, len);
			break;
		case ABO_AND:
			sign &= sign2;
			bytestr_and(s1, s2, len);
			break;
		case ABO_XOR:
			sign ^= sign2;
			bytestr_xor(s1, s2, len);
			break;
		case ABO_PLUS:
			carry = bytestr_add(s1, s2, len);
			sign ^= (sign ^ sign2) & (sign ^ !carry);
			break;
		case ABO_MINUS:
			carry = bytestr_sub(s1, s2, len);
			sign ^= ((sign ^ sign2) | (sign ^ !carry)) ^ 1;
			break;
		case ABO_MUL:
			sign ^= sign2;
			bytestr_mul(s1, s2, len);
			break;
		case ABO_DIV:
		case ABO_MOD:
			sign ^= sign2;
			if (! (smod = malloc(len)) ) {
				flags1 = HED_AEF_ERROR;
				break;
			}
			bytestr_div(s1, s2, len, smod);
			if (op == ABO_MOD)
				memcpy(s1, smod, len);
			free(smod);
			break;
		case ABO_SHL:
			bytestr_shl(s1, s2, len);
			break;
		case ABO_SHR:
			bytestr_shr(s1, s2, len);
			break;
		default:
			assert(0);
			break;
	}
	ret |= HED_AEF_NEGATIVE & -sign;
	return ret;
}

static void *
alloc_atom(enum atom_type type, size_t len)
{
	static const size_t sizes[] = {
		sizeof(struct atom_un),
		sizeof(struct atom_bin),
		sizeof(struct atom_bytestr),
		sizeof(struct atom_register),
		sizeof(struct atom_mark)
	};
	size_t asize = sizes[type];
	struct atom *a;

	if (type == ATOM_BYTESTR)
		asize += len;

	a = calloc(1, asize);
	if (a) {
		a->len = len;
		a->type = type;
	}
	return a;
}

static struct atom *
parse_un(char **sexpr, enum atom_un_op op)
{
	struct atom_un *a;
	struct atom *sa;

	skip_white(sexpr);
	sa = val_parse(sexpr);
	if (!sa)
		return NULL;

	a = alloc_atom(ATOM_UN, sa->len);
	if (!a) {
		free_atom(sa);
		return NULL;
	}
	a->op = op;
	a->atom = sa;
	return &a->a;
}

static struct atom *
parse_register(char **sexpr)
{
	struct atom_register *a;
	char reg = *(*sexpr)++;

	a = alloc_atom(ATOM_REGISTER, SIZE_AUTO);
	if (!a)
		return NULL;
	a->reg = reg;

	INIT_LIST_HEAD(&a->offset.atoms);
	if (**sexpr == '[') {
		(*sexpr)++;
		if (!compile_till(&a->offset, sexpr, ']')) {
			free(a);
			return NULL;
		}
		(*sexpr)++;
	}

	return &a->a;
}

static struct atom *
parse_mark(char **sexpr)
{
	struct atom_mark *a;
	char mark = *(*sexpr)++;

	a = alloc_atom(ATOM_MARK, sizeof(hed_uoff_t));
	if (!a)
		return NULL;
	a->mark = mark;
	return &a->a;
}

static struct atom *
parse_string(char **sexpr)
{
	struct atom_bytestr *a;
	char *base;
	size_t i = 0;

	base = *sexpr;
	while (**sexpr && **sexpr != '\'') {
		if (**sexpr == '\\') {
			(*sexpr)++;
			if (!**sexpr)
				return NULL;
		}
		i++;
		(*sexpr)++;
	}

	a = alloc_atom(ATOM_BYTESTR, i);
	if (!a)
		return NULL;
	for (i = 0; base < *sexpr; i++, base++) {
		if (*base == '\\')
			base++;
		a->bytes[i] = *base;
	}
	(*sexpr)++;
	return &a->a;
}

static struct atom *
parse_hexstr(char **sexpr)
{
	struct atom_bytestr *a;
	char *base;
	size_t i;

	base = *sexpr;
	while (isxdigit(**sexpr))
		(*sexpr)++;

	a = alloc_atom(ATOM_BYTESTR, (*sexpr - base + 1)/2);
	if (!a)
		return NULL;

	i = 0;
	if ((*sexpr - base) % 2)
		a->bytes[i++] = unhex(*base++);
	while (base < *sexpr) {
		a->bytes[i++] = (unhex(base[0]) << 4) + unhex(base[1]);
		base += 2;
	}
	return &a->a;
}

static struct atom_bytestr *
parse_number_hex(char **sexpr)
{
	char *p, *q;
	size_t len;
	unsigned char *s;
	unsigned shift;
	struct atom_bytestr *ret;

	for (q = p = *sexpr; isxdigit(*q); ++q);
	*sexpr = q;
	len = ((size_t)(q - p) + 1) >> 1;
	if (!len)
		return NULL;
	if (! (ret = alloc_atom(ATOM_BYTESTR, len)) )
		return ret;

	s = BYTESTR_LSB(ret->bytes, *plen);
	shift = 0;
	while (q-- > p) {
		*s |= unhex(*q) << shift;
		shift += 4;
		if (shift >= 8) {
			shift -= 8;
			s += BYTESTR_STEP;
		}
	}

	return ret;
}

static struct atom_bytestr *
parse_number_bin(char **sexpr)
{
	char *p, *q;
	size_t len;
	unsigned char *s;
	unsigned shift;
	struct atom_bytestr *ret;

	for (q = p = *sexpr; *q == '0' || *q == '1'; ++q);
	*sexpr = q;
	len = ((size_t)(q - p) + 7) >> 3;
	if (!len)
		return NULL;
	if (! (ret = alloc_atom(ATOM_BYTESTR, len)) )
		return ret;

	s = BYTESTR_LSB(ret->bytes, *plen);
	shift = 0;
	while (q-- > p) {
		*s |= (*q - '0') << shift;
		if (++shift >= 8) {
			shift -= 8;
			s += BYTESTR_STEP;
		}
	}

	return ret;
}

static struct atom_bytestr *
parse_number_oct(char **sexpr)
{
	char *p, *q;
	size_t len;
	unsigned char *s;
	unsigned shift, acc;
	struct atom_bytestr *ret;

	for (q = p = *sexpr; *q >= '0' && *q <= '7'; ++q);
	*sexpr = q;
	len = ((size_t)(q - p) * 3 + 7)/8 ?: 1;
	if (! (ret = alloc_atom(ATOM_BYTESTR, len)) )
		return ret;

	s = BYTESTR_LSB(ret->bytes, len);
	shift = acc = 0;
	while (q-- > p) {
		acc |= (*q - '0') << shift;
		shift += 3;
		if (shift >= 8) {
			shift -= 8;
			*s = acc;
			acc >>= 8;
			s += BYTESTR_STEP;
		}
	}
	if (acc)
		*s = acc;

	return ret;
}

/* Number of bits per decimal digit scaled by 256 (8 bit shift) */
#define SBITSPERDECDIG	851

static struct atom_bytestr *
parse_number_dec(char **sexpr)
{
	char *p, *q;
	size_t len;
	unsigned char *s;
	struct atom_bytestr *ret;

	/* We must make sure that the resulting bytestr will fit into
	 * the allocated space, so we must always round to the nearest
	 * higher integer. Because of this, the approximation of log2(10)
	 * and the dependency on the actual number, the allocated space
	 * may be larger than necessary.
	 */
	for (p = q = *sexpr; isdigit(*q); ++q);
	*sexpr = q;
	len = (((size_t)(q - p) * SBITSPERDECDIG + 255)/256 + 7)/8;
	if (! (ret = alloc_atom(ATOM_BYTESTR, len)) )
		return ret;

	while (p < q) {
		unsigned carry = *p++ - '0';
		for (s = BYTESTR_LSB(ret->bytes, len);
		     s != BYTESTR_MSB(ret->bytes, len) + BYTESTR_STEP;
		     s += BYTESTR_STEP) {
			carry += *s * 10;
			*s = carry;
			carry >>= 8;
		}
	}

	return ret;
}

static struct atom *
parse_number(char **sexpr)
{
	struct atom_bytestr *a;

	if (**sexpr == '0') {
		++(*sexpr);
		if ((**sexpr == 'x' || **sexpr == 'X')) {
			++(*sexpr);
			a = parse_number_hex(sexpr);
		} else if ((**sexpr == 'b' || **sexpr == 'B')) {
			++(*sexpr);
			a = parse_number_bin(sexpr);
		} else
			a = parse_number_oct(sexpr);
	} else
		a = parse_number_dec(sexpr);

	if (!a)
		return NULL;
	bytestr_shrink(a->bytes, &a->a.len);
	return &a->a;
}

static struct atom *
val_parse(char **sexpr)
{
	static const char uopc[] = "~-"; // in atom_un_op order
	char *uop;
	struct atom *ret = NULL;

	if ( (uop = strchr(uopc, **sexpr)) ) {
		(*sexpr)++;
		ret = parse_un(sexpr, uop - uopc);
	} else if (**sexpr == '"') {
		(*sexpr)++;
		ret = parse_register(sexpr);
	} else if (**sexpr == '@') {
		(*sexpr)++;
		ret = parse_mark(sexpr);
	} else if (**sexpr == '\'') {
		(*sexpr)++;
		ret = parse_string(sexpr);
	} else if (**sexpr == 'x') {
		(*sexpr)++;
		ret = parse_hexstr(sexpr);
	} else if (isdigit(**sexpr)) {
		ret = parse_number(sexpr);
	}

	if (!ret)
		return NULL;

	/* Modifiers */
	if (**sexpr == '.')
		(*sexpr)++;
	switch (**sexpr) {
	case 'B':
	case 'b':
		ret->len = 1;
		(*sexpr)++;
		break;
	case 'W':
	case 'w':
		ret->len = 2;
		(*sexpr)++;
		break;
	case 'D':
	case 'd':
	case 'L':
	case 'l':
		ret->len = 4;
		(*sexpr)++;
		break;
	case 'Q':
	case 'q':
		ret->len = 8;
		(*sexpr)++;
		break;
	case '{':
		ret->len = strtoull(*sexpr, sexpr, 0);
		if (**sexpr != '}') {
			free_atom(ret);
			return NULL;
		}
		(*sexpr)++;
		break;
	}
	return ret;
}

/* return the precedence of the operator @op */
static inline int
precedence(enum atom_bin_op op)
{
	static const int op2prec[] = {
		[ABO_LPAREN] = -1,
		[ABO_RPAREN] = 0,
		[ABO_OR] = 100,
		[ABO_XOR] = 110,
		[ABO_AND] = 120,
		[ABO_SHL] = 130,
		[ABO_SHR] = 130,
		[ABO_PLUS] = 140,
		[ABO_MINUS] = 140,
		[ABO_MUL] = 150,
		[ABO_DIV] = 150,
		[ABO_MOD] = 150,
	};
	assert(op >= 0 && op < sizeof(op2prec)/sizeof(int));
	return op2prec[op];
}

/* Reduce the stack up to an operator with a higher precedence than @till
 * Returns the first non-reduced atom, or NULL if there are no more atoms
 * on @stack.
 */
static struct atom_bin *
reduce_stack(struct hed_expr_priv *expr, struct list_head *stack, int minprec)
{
	while (!list_empty(stack)) {
		struct atom_bin *bin;

		bin = list_entry(stack->next, struct atom_bin, a.list);
		if (precedence(bin->op) < minprec)
			return bin;

		if (expr->atoms.prev == expr->atoms.next)
			return bin; /* zero or one element */

		bin->atom2 = list_entry(expr->atoms.prev, struct atom, list);
		list_del(&bin->atom2->list);
		bin->atom1 = list_entry(expr->atoms.prev, struct atom, list);
		list_del(&bin->atom1->list);

		list_move_tail(&bin->a.list, &expr->atoms);

		/* Note that BOTH may be SIZE_AUTO, but that's ok */
		if (bin->atom1->len == SIZE_AUTO)
			bin->a.len = bin->atom1->len = bin->atom2->len;
		else if (bin->atom2->len == SIZE_AUTO)
			bin->a.len = bin->atom2->len = bin->atom1->len;
		else
			bin->a.len = bin->atom1->len >= bin->atom2->len
				? bin->atom1->len : bin->atom2->len;
	}

	return NULL;
}

static struct hed_expr_priv *
compile_till(struct hed_expr_priv *expr, char **sexpr, char till)
{
	static const char bopc[] = "|&^+-*/%<>"; // in atom_bin_op order
	char *bop;

	struct atom *a;
	struct atom_bin *bin;
	struct list_head binstack;

	INIT_LIST_HEAD(&binstack);

	while (**sexpr != till) {
		skip_white(sexpr);

		if (**sexpr == '(') {
			bin = alloc_atom(ATOM_BIN, 0);
			if (!bin)
				goto err;
			bin->op = ABO_LPAREN;
			list_add(&bin->a.list, &binstack);
			(*sexpr)++;
			continue;
		}

		a = val_parse(sexpr);
		if (!a)
			goto err;
		list_add_tail(&a->list, &expr->atoms);

		skip_white(sexpr);
		if (**sexpr == ')') {
			bin = reduce_stack(expr, &binstack,
					   precedence(ABO_RPAREN));
			if (!bin)
				goto err; /* mismatched parenthesis */
			list_del(&bin->a.list);
			free_atom(&bin->a);
			(*sexpr)++;
		}

		bop = strchr(bopc, **sexpr);
		if (!bop || !*bop)
			continue;

		if (*bop == '<' || *bop == '>') {
			(*sexpr)++;
			if (**sexpr != *bop)
				goto err;
		}

		bin = alloc_atom(ATOM_BIN, 0);
		if (!bin)
			goto err;
		bin->op = bop - bopc;
		reduce_stack(expr, &binstack, precedence(bin->op));
		list_add(&bin->a.list, &binstack);
		(*sexpr)++;
	}

	if (reduce_stack(expr, &binstack, precedence(ABO_RPAREN)))
		goto err;

	list_for_each_entry(a, &expr->atoms, list) {
		if (a->len == SIZE_AUTO)
			a->len = 1;
		expr->e.len += a->len;
	}

	if (expr->e.len) {
		expr->e.buf = realloc(expr->e.buf, expr->e.len);
		if (!expr->e.buf)
			goto err;
	}

	return expr;

err:
	list_splice(&binstack, &expr->atoms);
	expr_cleanup(expr);
	return NULL;
}

struct hed_expr *
hed_expr_new(char *sexpr,
	     hed_expr_reg_cb reg_cb, hed_expr_mark_cb mark_cb, void *cb_data)
{
	struct hed_expr_priv *expr = calloc(1, sizeof(struct hed_expr_priv));

	if (!expr)
		return NULL;

	expr->reg_cb = reg_cb;
	expr->mark_cb = mark_cb;
	expr->cb_data = cb_data;
	INIT_LIST_HEAD(&expr->atoms);

	if (compile_till(expr, &sexpr, '\0'))
		return &expr->e;

	free(expr);
	return NULL;
}

static long
atom_un(struct hed_expr_priv *expr, struct atom *atom,
	unsigned char *scramble, size_t len)
{
	struct atom_un *data = (struct atom_un *) atom;
	long flags;
	assert(data);
	assert(data->atom);
	flags = atom_eval(expr, data->atom, scramble, len);
	if (flags & HED_AEF_ERROR)
		return flags;
	return bytestr_unop(scramble, len, flags, data->op);
}

static long
do_atom_bin(struct hed_expr_priv *expr, struct atom_bin *data,
	    unsigned char *scramble, size_t len, unsigned char *tmp)
{
	long flags1, flags2;
	assert(data);
	assert(data->atom1 && data->atom2);

	flags1 = atom_eval(expr, data->atom1, scramble, len);
	if (flags1 & HED_AEF_ERROR)
		return flags1;

	flags2 = atom_eval(expr, data->atom2, tmp, len);
	if (flags2 & HED_AEF_ERROR)
		return flags2;

	return bytestr_binop(scramble, tmp, len,
			     flags1, flags2, data->op);
}

static long
atom_bin(struct hed_expr_priv *expr, struct atom *atom,
	 unsigned char *scramble, size_t len)
{
	struct atom_bin *data = (struct atom_bin *) atom;
	unsigned char *tmp;
	long ret;

	tmp = malloc(len);
	if (!tmp)
		return HED_AEF_ERROR;

	ret = do_atom_bin(expr, data, scramble, len, tmp);

	free(tmp);
	return ret;
}

static long
atom_bytestr(struct hed_expr_priv *expr, struct atom *atom,
	     unsigned char *scramble, size_t len)
{
	struct atom_bytestr *data = (struct atom_bytestr *) atom;
	assert(data);
	if (len > data->a.len) {
		memcpy(BYTESTR_LOPART(scramble, len, data->a.len),
		       data->bytes, data->a.len);
		memset(BYTESTR_HIPART(scramble, len, len - data->a.len),
		       0, len - data->a.len);
	} else
		memcpy(scramble,
		       BYTESTR_LOPART(data->bytes, data->a.len, len), len);
	return 0;
}

static long
atom_mark(struct hed_expr_priv *expr, struct atom *atom,
	  unsigned char *scramble, size_t len)
{
	struct atom_mark *data = (struct atom_mark *) atom;
	assert(data);
	if (!expr->mark_cb) {
		memset(scramble, 0, len);
		return 0;
	}

	return expr->mark_cb(expr->cb_data, data->mark, scramble, len);
}

static long
atom_register(struct hed_expr_priv *expr, struct atom *atom,
	      unsigned char *scramble, size_t len)
{
	struct atom_register *data = (struct atom_register *) atom;
	hed_uoff_t ofs;
	assert(data);
	if (!expr->reg_cb) {
		memset(scramble, 0, len);
		return 0;
	}

	data->offset.reg_cb = expr->reg_cb;
	data->offset.mark_cb = expr->mark_cb;
	data->offset.cb_data = expr->cb_data;
	if (hed_expr_eval(&data->offset.e) & HED_AEF_ERROR)
		return HED_AEF_ERROR;

	ofs = hed_expr2off(&data->offset.e);

	if (data->reg == '.')
		ofs += expr->cb_pos;
	return expr->reg_cb(expr->cb_data, data->reg, ofs,
			    scramble, len);
}

static long
atom_eval(struct hed_expr_priv *expr, struct atom *atom,
	  unsigned char *scramble, size_t len)
{
	if (!len)
		return 0;

	switch (atom->type) {
	case ATOM_UN: return atom_un(expr, atom, scramble, len);
	case ATOM_BIN: return atom_bin(expr, atom, scramble, len);
	case ATOM_BYTESTR: return atom_bytestr(expr, atom, scramble, len);
	case ATOM_REGISTER: return atom_register(expr, atom, scramble, len);
	case ATOM_MARK: return atom_mark(expr, atom, scramble, len);
	default: assert(0); return HED_AEF_ERROR;
	}
}

long
hed_expr_eval(struct hed_expr *e)
{
	struct hed_expr_priv *expr = (struct hed_expr_priv*)
		((char*) e - offsetof(struct hed_expr_priv, e));
	struct atom *a;

	expr->cb_pos = 0;
	expr->e.flags = 0;

	assert(expr && expr->e.len >= 0);
	list_for_each_entry (a, &expr->atoms, list) {
		long flags;

		flags = atom_eval(expr, a,
				  expr->e.buf + expr->cb_pos, a->len);
		expr->e.flags |= (flags & (HED_AEF_DYNAMIC | HED_AEF_ERROR));
		if (!expr->cb_pos)
			expr->e.flags |= (flags & HED_AEF_NEGATIVE);
		else
			expr->e.flags &= ~HED_AEF_NEGATIVE;
		expr->cb_pos += a->len;
	}
	assert(expr->cb_pos == expr->e.len);
	return expr->e.flags;
}

static void
cleanup_un(struct atom_un *un)
{
	free_atom(un->atom);
}

static void
cleanup_bin(struct atom_bin *bin)
{
	if (bin->atom1)
		free_atom(bin->atom1);
	if (bin->atom2)
		free_atom(bin->atom2);
}

static void
cleanup_register(struct atom_register *reg)
{
	expr_cleanup(&reg->offset);
}

static void
free_atom(struct atom *atom)
{
	if (atom->type == ATOM_UN)
		cleanup_un((struct atom_un *) atom);
	else if (atom->type == ATOM_BIN)
		cleanup_bin((struct atom_bin *) atom);
	else if (atom->type == ATOM_REGISTER)
		cleanup_register((struct atom_register*) atom);

	free(atom);
}

static void
expr_cleanup(struct hed_expr_priv *expr)
{
	struct atom *a, *next;

	list_for_each_entry_safe (a, next, &expr->atoms, list)
		free_atom(a);
	if (expr->e.buf)
		free(expr->e.buf);
}

void
hed_expr_free(struct hed_expr *e)
{
	struct hed_expr_priv *expr = (struct hed_expr_priv*)
		((char*) e - offsetof(struct hed_expr_priv, e));
	expr_cleanup(expr);
	free(expr);
}
