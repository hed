/* A splay tree implemenatation for file blocks */

/* This is a libhed PRIVATE file.
 * Do not include outside of libhed. Do not install on the system.
 */

#ifndef LIBHED__TREE_H
#define LIBHED__TREE_H

#include "types.h"
#include <util/lists.h>

/* Prevent polluting linker namespace with internal symbols */
#include "private.h"
#define del_from_tree internal_name(del_from_tree)
#define find_in_tree internal_name(find_in_tree)
#define init_tree internal_name(init_tree)
#define insert_into_tree internal_name(insert_into_tree)
#define recalc_del_from_tree internal_name(recalc_del_from_tree)
#define recalc_node_recursive internal_name(recalc_node_recursive)
#define tree_block_offset internal_name(tree_block_offset)

/* Behold, a tree! */
struct hed_tree {
	struct hed_tree_node *root;
};

/**
 * tree_entry - get the struct for this entry
 * @ptr:	the &struct hed_tree_node pointer.
 * @type:	the type of the struct this is embedded in.
 * @member:	the name of the struct hed_tree_node within the struct.
 */
#define tree_entry(ptr, type, member) ({			\
	const struct hed_tree_node *__mptr = (ptr);			\
	(type *)( (char *)__mptr - offsetof(type,member) );})

#define tree_list_entry(ptr)	list_entry(ptr,struct hed_tree_node,link)

/* THIS IS NOT AN IMPLEMENTATION OF A GENERIC SPLAY TREE
 * 
 * The tree structure implemented here has some limitations which
 * do not matter to the only use case (struct hed_file), but allow
 * simplifying the code.
 *
 * 1. The tree always contains at least one node
 *    Code that relies on this feature is tagged with the NONEMPTY keyword.
 *
 * 2. The last node in the tree does not change
 *    A pointer to this node is given during initialization already,
 *    and it must stay valid for the whole lifetime of the tree.
 *    Code that relies on this feature is tagged with the CONSTLAST keyword.
 */

/* These prototypes may be in fact defined as macros below! */

/* Initialize a new tree with one element (the tree @root), which is
 * the immutable last element of the tree.
 */
void init_tree(struct hed_tree *tree, struct hed_tree_node *root);

#define init_node(node)	INIT_LIST_HEAD(&(node)->link)

/* Update node information */
void recalc_node_recursive(struct hed_tree_node *item);

/* Delete from the tree, but do NOT recalculate parent nodes. Use this when
 * replacing one node with another (equally sized). However, you SHOULD
 * recalculate when merging two nodes together etc. - the cover sizes
 * might not be right if you merged a child in. */
void del_from_tree(struct hed_tree *tree, struct hed_tree_node *item);

/* Delete from the tree and recalculate parent nodes. */
void recalc_del_from_tree(struct hed_tree *tree, struct hed_tree_node *item);

/* Insert a new item before @pos.
 * Inserting at the end (appending to the tree) is not possible.
 */
void insert_into_tree(struct hed_tree *tree, struct hed_tree_node *item,
		      struct hed_tree_node *pos);

/* Finds the item at or right before the given offset (except when there is
 * no item at or before the offset; then it gives the nearest item after
 * the offset; you can distinguish the case by item->left being the
 * null node. */
struct hed_tree_node *find_in_tree(struct hed_tree *tree, hed_uoff_t offset);

#define prev_in_tree(item)	tree_list_entry((item)->link.prev)
#define next_in_tree(item)	tree_list_entry((item)->link.next)

hed_uoff_t tree_block_offset(struct hed_tree_node *block);

#endif
