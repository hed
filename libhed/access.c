/* Low-level file access */
/* $Id$ */

/*
 * hed - Hexadecimal editor
 * Copyright (C) 2004  Petr Baudis <pasky@ucw.cz>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of version 2 of the GNU General Public License as
 * published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/* The idea here is that an mmapped region may get beyond EOF, e.g. if
 * the file gets truncated by another process.  When accessing such
 * areas, the kernel generates a SIGBUS.  Our signal handler replaces
 * the faulty page with a new mapping.  The page is still tracked by
 * the cache, so it will be freed when the cache object is freed.
 */

#include <config.h>

#include <signal.h>
#include <unistd.h>

#include "access.h"
#include "cache.h"

#if HED_USES_MMAP

#include <sys/mman.h>
#include <stdint.h>

unsigned int sys_page_shift;
unsigned long sys_page_size;

#endif

#ifdef HED_CONFIG_MMAP

#include "file_priv.h"

LIST_HEAD(fixup_files);

static struct hed_block_data *
find_bdata(void *addr)
{
	struct file_priv *file;

	list_for_each_entry(file, &fixup_files, fixup_list) {
		struct hed_cache *cache = file->cache;
		int i;
		for (i = 0; i < cache->nelem; ++i)
			if (cache->revmap[i]->data == addr)
				return cache->revmap[i];
	}
	return NULL;
}

static void
sigbus_handler(int sig, siginfo_t *si, void *ctx)
{
	void *page = (void*)(intptr_t)FILE_BLOCK_ROUND((intptr_t)si->si_addr);
	struct hed_block_data *bdata = find_bdata(page);

	if (bdata) {
		bdata->data = mmap(page, sys_page_size,
				   PROT_READ | PROT_WRITE,
				   MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED,
				   -1, 0);
	} else {
		/* Restart the instruction and fail */
		signal(SIGBUS, SIG_DFL);
	}
}

#endif	/* HED_CONFIG_MMAP */

#if defined(HED_CONFIG_MMAP) || defined(HED_CONFIG_SWAP_MMAP)

int
file_access_init(void)
{
	sys_page_size = sysconf(_SC_PAGESIZE);
	sys_page_shift = bitsize(sys_page_size);
	/* FILE_BLOCK_* macros only work with page sizes which
	 * are a power of two. Abort if this is not the case. */
	assert(FILE_BLOCK_SIZE == FILE_BLOCK_ROUND(FILE_BLOCK_SIZE));
	assert(SWAP_BLOCK_SIZE >= FILE_BLOCK_SIZE);

#ifdef HED_CONFIG_MMAP
	struct sigaction sa;

	sa.sa_flags = SA_SIGINFO;
	sa.sa_sigaction = sigbus_handler;
	sigemptyset(&sa.sa_mask);

	return sigaction(SIGBUS, &sa, NULL);
#else
	return 0;
#endif
}

#endif
