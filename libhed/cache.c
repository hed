/*
 * hed - Hexadecimal editor
 * Copyright (C) 2008  Petr Tesarik <petr@tesarici.cz>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of version 2 of the GNU General Public License as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <config.h>

#include <stdlib.h>
#include <string.h>

#include <util/lists.h>
#include "cache.h"
#include "swap.h"
#include "access.h"

#undef CACHE_DEBUG
#ifdef CACHE_DEBUG
# include <stdio.h>
# define CDEBUG(x...) fprintf(stderr, x)
#else
# define CDEBUG(x...)
#endif

/* Return the size (in bytes) of a cache with @nelem entries.
 */
static inline size_t
cache_size(unsigned nelem)
{
	return sizeof(struct hed_cache)
		+ nelem * (sizeof(struct hed_block_data*)	/* revmap */
			   + sizeof(struct hed_block_data)	/* objects */
			   + FILE_BLOCK_ALLOC);			/* data */
}

struct hed_cache *
cache_init(unsigned nelem, struct swp_file *swp)
{
	struct hed_cache *cache;
	struct hed_block_data **revmap, *data;
	void *pdata;
	struct list_head *plink;

	cache = malloc(cache_size(nelem));
	if (!cache)
		return NULL;

	CDEBUG("Initialize a cache at %p with %d elements:\n", cache, nelem);

	revmap = (void*)cache + sizeof(struct hed_cache);
	data = (void*)revmap + nelem * sizeof(struct hed_block_data*);
	pdata = (void*)data + nelem * sizeof(struct hed_block_data);

	cache->nelem = nelem;
	cache->nfree = nelem;
	cache->revmap = revmap;
	cache->swp = swp;
	cache->data = pdata;
	plink = &cache->free;
	while (nelem--) {
		CDEBUG("  block data at %p\n", data);

		plink->next = &data->u2.free;
		data->size = FILE_BLOCK_SIZE;
		if (FILE_BLOCK_ALLOC) {
			data->data = pdata;
			pdata += FILE_BLOCK_ALLOC;
		} else
			data->data = NULL;
		data->u2.free.prev = plink;
		plink = &data->u2.free;
		*revmap++ = data++;
	}

	plink->next = &cache->free;
	cache->free.prev = plink;
	return cache;
}

void
cache_done(struct hed_cache *cache)
{
	free(cache);
}

struct hed_block_data *
cache_alloc(struct hed_cache *cache)
{
	struct hed_block_data *item;

	CDEBUG("Allocating a new cache object... ");

	if (list_empty(&cache->free)) {
		CDEBUG("failed (cache full)\n");
		return NULL;
	}
	item = list_entry(cache->free.next, struct hed_block_data, u2.free);

	CDEBUG("ok, allocated at %p\n", item);

	/* Initialize a not-in-use entry */
	list_del(&item->u2.free);
	item->u2.used.refcount = 1;
	item->u2.used.flags = HED_FDF_CACHED;

	--cache->nfree;

	return item;
}

#if defined(HED_CONFIG_READAHEAD) && !defined(HED_CONFIG_MMAP)

/* Make data slot at @srcidx available for re-use by relocating
 * the data for its corresponding entry to data slot at @dstidx.
 * WARNING: This function does not re-initialize the dest entry.
 * That must be done by the caller.
 */
static void
move_entry(struct hed_cache *cache, unsigned srcidx, unsigned dstidx)
{
	struct hed_block_data **revmap = cache->revmap;
	struct hed_block_data *src = revmap[srcidx];
	struct hed_block_data *dst = revmap[dstidx];
	void *data;

	CDEBUG("Moving data slot %d to %d\n", srcidx, dstidx);

	revmap[dstidx] = src;
	data = src->data;
	src->data = dst->data;
	memcpy(dst->data, data, FILE_BLOCK_SIZE);
}

#ifdef CACHE_DEBUG

static void
dump_usemap(unsigned char *map, unsigned nelem)
{
	unsigned i;

	for (i = 0; i < nelem; ++i)
		putc(map[i] ? '#' : '.', stderr);
	putc('\n', stderr);
}

#else  /* CACHE_DEBUG */

#define dump_usemap(u,n)	do{} while(0)

#endif

static unsigned
reorder_elements(struct hed_cache *cache, struct hed_block_data **items, int n)
{
	void *base = cache->data;
	unsigned char used[2*n];
	unsigned pool[2*n];
	int npool, npoolhi;
	int hi, usedoff;
	int i;

	memset(used, 0, sizeof used);
	hi = cache->nelem - n;
	npool = npoolhi = 0;
	for (i = 0; i < n; ++i) {
		unsigned idx = (items[i]->data - base) >> FILE_BLOCK_SHIFT;
		if (idx < n)
			used[idx] = 1;
		else
			pool[npool++] = idx;
		if (idx >= hi)
			used[n + idx - hi] = 1;
		else
			pool[n + npoolhi++] = idx;
	}

	CDEBUG("Reorder %d elements:\n", n);
	CDEBUG("lo: "); dump_usemap(used, n);
	CDEBUG("hi: "); dump_usemap(used + n, n);

	if (npoolhi < npool) {
		npool = n + npoolhi;
		usedoff = n;
	} else
		usedoff = hi = 0;

	for (i = 0; i < n; ++i)
		if (!used[i + usedoff])
			move_entry(cache, i + hi, pool[--npool]);
	assert(npool == usedoff);

	return hi;
}

void
cache_compact(struct hed_cache *cache, struct hed_block_data **items, int n)
{
	void *lower, *upper, *p;
	unsigned firstslot;
	int i;

	lower = upper = items[0]->data;
	for (i = 1; i < n; ++i) {
		assert(items[i]->u2.used.flags & HED_FDF_CACHED);
		if (items[i]->data < lower)
			lower = items[i]->data;
		else if (items[i]->data > upper)
			upper = items[i]->data;
	}

	if (upper - lower >= (n << FILE_BLOCK_SHIFT)) {
		firstslot = reorder_elements(cache, items, n);
		p = cache->data + (firstslot << FILE_BLOCK_SHIFT);
	} else {
		firstslot = (lower - cache->data) >> FILE_BLOCK_SHIFT;
		p = lower;
	}

	for (i = 0; i < n; ++i) {
		items[i]->data = p;
		cache->revmap[i + firstslot] = items[i];
		p += FILE_BLOCK_SIZE;
	}
}

#endif

struct hed_block_data *
block_data_new(struct hed_cache *cache, size_t size)
{
	struct swp_file *swp = cache->swp;
	struct hed_block_data *item =
		swp_zalloc(swp, sizeof(struct hed_block_data));

	CDEBUG("Allocated an out-of-cache object: %p\n", item);

	if (!item)
		return NULL;

	item->size = size;
	if (!(item->data = swp_malloc(swp, size))) {
		swp_free(swp, item);
		return NULL;
	}
	item->u2.used.refcount = 1;

	return item;
}

void
block_data_shrink(struct hed_cache *cache,
		  struct hed_block_data *data, size_t newsize)
{
	CDEBUG("Shrinking object at %p from %d to %d\n",
	       data, data->size, newsize);
	assert(newsize <= data->size);

	data->size = newsize;
	data->data = swp_shrink(cache->swp, data->data, newsize);
}

/* This function can handle both cached and out-of-cache objects. */
void
_block_data_free(struct hed_cache *cache, struct hed_block_data *data)
{
	CDEBUG("Releasing object at %p: ", data);
	if (! (data->u2.used.flags & HED_FDF_CACHED)) {
		struct swp_file *swp = cache->swp;
		CDEBUG("deallocated\n");
		if (data->data)
			swp_free(swp, data->data);
		swp_free(swp, data);
	} else {
		CDEBUG("returned to cache\n");

		list_add_tail(&data->u2.free, &cache->free);
		++cache->nfree;
	}
}
