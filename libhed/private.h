/* This is a libhed PRIVATE file.
 * Do not include outside of libhed. Do not install on the system.
 */

#ifndef LIBHED__PRIVATE_H
#define LIBHED__PRIVATE_H

#define internal_name(n)	hed__internal__ ## n

#endif	/* LIBHED__PRIVATE_H */
