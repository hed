/* $Id$ */

/*
 * hed - Hexadecimal editor
 * Copyright (C) 2004  Petr Baudis <pasky@ucw.cz>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of version 2 of the GNU General Public License as
 * published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * When his eyes were in turn uncovered, Frodo looked up and caught his breath.
 * They were standing in an open space. To the left stood a great mound,
 * covered with a sward of grass as green as Spring-time in the Elder Days.
 * Upon it, as a double crown, grew two circles of trees: the outer had bark of
 * snowy white, and were leafless but beautiful in their shapely nakedness; the
 * inner were mallorn-trees of great height, still arrayed in pale gold. High
 * amid the branches of a towering tree that stood in the centre of all there
 * gleamed a white flet. At the feet of the trees, and all about the green
 * hillsides the grass was studded with small golden flowers shaped like stars.
 * Among them, nodding on slender stalks, were other flowers, white and palest
 * green: they glimmered as a mist amid the rich hue of the grass. Over all the
 * sky was blue, and the sun of afternoon glowed upon the hill and cast long
 * green shadows beneath the trees.
 */

#include <config.h>

#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <assert.h>

#include "tree.h"


/* Define to enable debug messages */
#undef TREE_DEBUG
#ifdef TREE_DEBUG
#define TDEBUG(x...) fprintf(stderr, x)
#else
#define TDEBUG(x...)
#endif

/* You shouldn't need to call these directly unless you want to
 * do special optimizations for certain items. */
static void splay(struct hed_tree *tree, struct hed_tree_node *item);
static void unsplay(struct hed_tree *tree, struct hed_tree_node *item);

void
init_tree(struct hed_tree *tree, struct hed_tree_node *root)
{
	init_node(root);
	root->up = root->left = root->right = NULL;
	tree->root = root;
}

static void
recalc_node(struct hed_tree_node *item)
{
	item->cover_size = item->size;
	if (item->left) {
		item->cover_size += item->left->cover_size;
		item->left->up = item;
	}
	if (item->right) {
		item->cover_size += item->right->cover_size;
		item->right->up = item;
	}
}

void
recalc_node_recursive(struct hed_tree_node *item)
{
	while (item) {
		recalc_node(item);
		item = item->up;
	}
}

static void
del_leaf(struct hed_tree_node *item)
{
	list_del(&item->link);

	/* NONEMPTY: no check if this is the last item in the tree */

	assert(!item->left && !item->right && item->up);
	if (item->up->left == item) {
		item->up->left = NULL;
	} else {
		assert(item->up->right == item);
		item->up->right = NULL;
	}

	item->up = NULL;
}

void
del_from_tree(struct hed_tree *tree, struct hed_tree_node *item)
{
	unsplay(tree, item);
	del_leaf(item);
}

void
recalc_del_from_tree(struct hed_tree *tree, struct hed_tree_node *item)
{
	struct hed_tree_node *parent;

	/*
	 * 'I'll give your name and number to the Nazgyl,' said the soldier
	 * lowering his voice to a hiss. 'One of *them*'s in charge at the
	 * Tower now.'
	 */

	unsplay(tree, item);
	parent = item->up;
	del_leaf(item);
	assert(item->size == item->cover_size);
	while (parent) {
		parent->cover_size -= item->size;
		parent = parent->up;
	}
}

void
insert_into_tree(struct hed_tree *tree, struct hed_tree_node *item,
		 struct hed_tree_node *pos)
{
	splay(tree, pos);
	list_add_tail(&item->link, &tree->root->link);

	item->right = pos;
	item->left = pos->left;
	pos->left = NULL;

	item->up = NULL;
	tree->root = item;

	recalc_node(pos);
	recalc_node(item);
}

struct hed_tree_node *
find_in_tree(struct hed_tree *tree, hed_uoff_t offset)
{
	struct hed_tree_node *item = NULL, *nitem = tree->root;
	hed_uoff_t pos = nitem->left ? nitem->left->cover_size : 0;
				/* NONEMPTY: nitem is always non-NULL */

	while (nitem) {
		TDEBUG("find_in_tree(%llx): %llx\n", offset, pos);
		item = nitem;
		if (pos <= offset && offset < pos + item->size) {
			TDEBUG("find_in_tree(%llx): match - pos %llx, size %llx\n", offset, pos, item->size);
			nitem = NULL;
		} else if (pos > offset) {
			TDEBUG("find_in_tree(%llx): going left\n", offset);
			nitem = item->left;
			pos -= nitem
				? nitem->size + (nitem->right
					? nitem->right->cover_size
					: 0)
				: 0;
		} else { assert(pos <= offset && pos + item->size <= offset);
			TDEBUG("find_in_tree(%llx): going right\n", offset);
			nitem = item->right;
			pos += item->size + (nitem && nitem->left
				? nitem->left->cover_size
				: 0);
			/* If we are going far too left, we must return NULL. */
			item = nitem;
		}
	}

	if (item)
		TDEBUG("find_in_tree(%llx): chosen %llx + %llx\n", offset, pos, item->size);
	else
		TDEBUG("find_in_tree(%llx): chosen %llx - but not found\n", offset, pos);

	/* TODO: Splay as we go? */
	if (item)
		splay(tree, item);

	return item;
}

hed_uoff_t
tree_block_offset(struct hed_tree_node *item)
{
	struct hed_tree_node *up;
	hed_uoff_t off;

	assert(item);
	off = item->left ? item->left->cover_size : 0;
	while ( (up = item->up) ) {
		if (item == up->right)
			off += up->cover_size - item->cover_size;
		item = up;
	} 
	return off;
}

static void
update_parent(struct hed_tree *tree,
	      struct hed_tree_node *old, struct hed_tree_node *new)
{
	new->up = old->up;
	if (!new->up) {
		assert(tree->root == old);
		tree->root = new;
	} else {
		if (old->up->left == old) {
			new->up->left = new;
		} else { assert(old->up->right == old);
			new->up->right = new;
		}
	}
}

static void
splay(struct hed_tree *tree, struct hed_tree_node *item)
{
	struct hed_tree_node *item_mid, *item_top;

	assert(item || item == tree->root);
	assert(tree->root || item == tree->root);

	while (item != tree->root) {
		item_top = tree->root;

		if (item->up == item_top) {
			if (item_top->left == item) {
				item_top->left = item->right;
				item->right = item_top;
			} else { assert(item_top->right == item);
				item_top->right = item->left;
				item->left = item_top;
			}
			recalc_node(item_top);
			recalc_node(item);
			tree->root = item;
			item->up = NULL;
			return;
		}

		/*
	 	* 'Come on, you slugs!' he cried. 'This is no time for
		* slouching.' He took a step towards them, and even in the
		* gloom he recognized the devices on their shields.
		* 'Deserting, eh?' he snarled. 'Or thinking of it? All your
		* folk should have been inside Udyn before yesterday evening.
		* You know that. Up you get and fall in, or I'll have your
	 	* numbers and report you.'
	 	*/

		assert(item->up);

		item_mid = item->up; assert(item_mid);
		item_top = item_mid->up; assert(item_top);

		update_parent(tree, item_top, item);
		item_top->up = item;

		if (item_top->left == item_mid &&
		    item_mid->left == item) {
			item_mid->left = item->right;
			item->right = item_top;
		} else if (item_top->right == item_mid &&
			   item_mid->right == item) {
			/* Inverse operation. */
			item_mid->right = item->left;
			item->left = item_top;
		} else if (item_top->left == item_mid &&
			   item_mid->right == item) {
			item_top->left = item->right;
			item->right = item_top;

			item_mid->up = item;
			item_mid->right = item->left;
			item->left = item_mid;
		} else { assert(item_top->right == item_mid &&
				item_mid->left == item);
			/* Inverse operation. */
			item_top->right = item->left;
			item->left = item_top;

			item_mid->up = item;
			item_mid->left = item->right;
			item->right = item_mid;
		}

		recalc_node(item_mid);
		recalc_node(item_top);
		recalc_node(item);
	}
}

static void
unsplay(struct hed_tree *tree, struct hed_tree_node *item)
{
	assert(tree->root);

	for (;;) {
		struct hed_tree_node *item_down;

		/* TODO: If we kept track of how many items are in each
		 * subtree, we could've made an actually inteligent
		 * choice. */
		if (item->left) {
			item_down = item->left;
			item->left = item_down->right;
			item_down->right = item;
		} else if(item->right) {
			item_down = item->right;
			item->right = item_down->left;
			item_down->left = item;
		} else
			break;

		update_parent(tree, item, item_down);
		item->up = item_down;
		recalc_node(item);
		recalc_node(item_down);
	}
}
