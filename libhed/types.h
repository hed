/* Basic libhed data types */

/* This is a libhed PUBLIC file.
 * This header file will be installed on the target system.
 * When you add new things here, make sure they start with hed_.
 */

/*
 * hed - Hexadecimal editor
 * Copyright (C) 2012  Petr Tesarik <petr@tesarici.cz>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of version 2 of the GNU General Public License as
 * published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef LIBHED__TYPES_H
#define LIBHED__TYPES_H

#include <libhed/config.h>

/*******************************************************************
 * UNSIGNED OFFSET TYPE
 */

#include <stdint.h>

/* If LFS is requested, define all offset types as 64-bit integers,
 * otherwise use the default size for the architecture
 */
#ifdef HED_CONFIG_LFS
typedef int64_t hed_off_t;
typedef uint64_t hed_uoff_t;
#else
typedef signed long int hed_off_t;
typedef unsigned long int hed_uoff_t;
#endif

/*******************************************************************
 * DOUBLY LINKED LIST
 */

struct hed_list_head {
	struct hed_list_head *next, *prev;
};

/*******************************************************************
 * FILE TREE
 */

struct hed_tree_node {
	/* Used for the linked list */
	struct hed_list_head link;
	/* Can be the null node for the root. */
	struct hed_tree_node *up;
	/* Both can be the null node for leaves. */
	struct hed_tree_node *left;
	struct hed_tree_node *right;
	/* Size of the entry. (This is public.) */
	hed_uoff_t size;
	/* Size of the entry plus its children. (Private.) */
	hed_uoff_t cover_size;
};

/*******************************************************************
 * BLOCK DATA
 */

/* This is the block data descriptor
 * The usage of some fields is not self-evident:
 *
 * @size	ALWAYS denotes the number of bytes which can fit
 *		into @data
 *
 * @data	may be embedded inside the hed_block_data object,
 *		detached in a dynamically allocated array,
 *		or unmaintained (for cached blocks with HED_CONFIG_MMAP)
 *
 * Some additional notes:
 * - @data is always allocated for out-of-cache objects
 */
struct hed_block_data {
	size_t size;		/* size of data */
	void *data;

	union {
		struct hed_list_head free; /* free list */
		struct {
			int refcount;	/* reference count */
			int flags;	/* flags (see HED_FDF_* below) */
		} used;
	} u2;
};

#define HED_FDF_CACHED	1	/* Allocated from cache */

#endif	/* LIBHED__TYPES_H */
