/* Low-level file accessors */

/* This is a libhed PRIVATE file.
 * Do not include outside of libhed. Do not install on the system.
 */

/*
 * hed - Hexadecimal editor
 * Copyright (C) 2012  Petr Tesarik <petr@tesarici.cz>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of version 2 of the GNU General Public License as
 * published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * Definitions in this file abstract from the low-level file access
 * method. There are currently two access methods:
 *   read()
 *   mmap()
 *
 * However, there might be more methods in the future, such as
 * a BIOS-call based one for making a standalone disk editor
 * directly loadable from the boot manager.
 */

#ifndef LIBHED__ACCESS_H
#define LIBHED__ACCESS_H

#include "config.h"

#include <assert.h>

/* Prevent polluting linker namespace with internal symbols */
#include "private.h"
#define sys_page_shift internal_name(sys_page_shift)
#define sys_page_size internal_name(sys_page_size)
#define file_access_init internal_name(file_access_init)

#define FILE_BLOCK_MASK		((signed long)FILE_BLOCK_SIZE-1)
#define FILE_BLOCK_OFF(x)	((hed_off_t)(x) & FILE_BLOCK_MASK)
#define FILE_BLOCK_ROUND(x)	((hed_off_t)(x) & ~FILE_BLOCK_MASK)
#define FILE_BLOCK_END(x)	((hed_off_t)(x) | FILE_BLOCK_MASK)

#if defined(HED_CONFIG_MMAP) || defined(HED_CONFIG_SWAP_MMAP)

#define HED_USES_MMAP	1	/* We use mmap() in some way */

extern unsigned int sys_page_shift;	/* Page size bits */
extern unsigned long sys_page_size;	/* Page size (in bytes) */

#endif

#ifdef HED_CONFIG_MMAP

#include <sys/mman.h>
#include <util/lists.h>
#include "file_priv.h"

#define FILE_BLOCK_SHIFT sys_page_shift
#define FILE_BLOCK_SIZE sys_page_size

/* Space to be allocated for each block's data */
#define FILE_BLOCK_ALLOC	0

struct remap_control {
	void *start;		/* start address of mapped region */
	void *end;		/* end address of mapped region */
};

static inline void
remap_init(struct remap_control *rc)
{
	rc->start = rc->end = NULL;
}

static inline void
remap_finish(struct remap_control *rc)
{
	if (rc->start != rc->end)
		munmap(rc->start, rc->end - rc->start);
}

#define remap_compact(rc,cache,preload,n) do { } while(0)

static inline void
remap_add(struct remap_control *rc, void *data)
{
	if (data == rc->end)
		rc->end = data + FILE_BLOCK_SIZE;
	else if (data + FILE_BLOCK_SIZE == rc->start)
		rc->start = data;
	else if (data) {
		remap_finish(rc);
		rc->start = data;
		rc->end = data + FILE_BLOCK_SIZE;
	}
}

#define fixup_files internal_name(fixup_files)
extern struct list_head fixup_files;

static inline void
fixup_register(struct file_priv *file)
{
	list_add(&file->fixup_list, &fixup_files);
}

static inline void
fixup_deregister(struct file_priv *file)
{
	list_del(&file->fixup_list);
}

#else  /* !HED_CONFIG_MMAP */

#include "cache.h"

/* More or less arbitrarily chosen: */
#define FILE_BLOCK_SHIFT	12
#define FILE_BLOCK_SIZE		(1U << FILE_BLOCK_SHIFT)

/* Space to be allocated for each block's data */
#define FILE_BLOCK_ALLOC	FILE_BLOCK_SIZE

struct remap_control { };

#define remap_init(rc)		do {} while(0)
#define remap_finish(rc)	do {} while(0)
#define remap_add(rc,data)	do {} while(0)

static inline void
remap_compact(struct remap_control *rc, struct hed_cache *cache,
	      struct hed_block_data **preload, int n)
{
#ifdef HED_CONFIG_READAHEAD
	cache_compact(cache, preload, n);
#endif
}

#define fixup_register(f)	do {} while(0)
#define fixup_deregister(f)	do {} while(0)

#endif	/* HED_CONFIG_MMAP */

#if HED_USES_MMAP

#include <unistd.h>
#include "util/numbers.h"

#define SWAP_BLOCK_SHIFT sys_page_shift
#define SWAP_BLOCK_SIZE sys_page_size

int file_access_init(void);

#else

#define SWAP_BLOCK_SHIFT FILE_BLOCK_SHIFT
#define SWAP_BLOCK_SIZE	FILE_BLOCK_SIZE

static inline int
file_access_init(void)
{
	return 0;
}

#endif

#endif	/* LIBHED__ACCESS_H */
