/*
 * memrchr - Scan memory backwards for a character
 * Copyright (C) 2011  Petr Tesarik <petr@tesarici.cz>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of version 2 of the GNU General Public License as
 * published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/* memrchr(3) is available with recent versions of glibc, but it may be
 * missing on other platforms, so provide a replacement
 */
static void *
memrchr(const void *s, int c, size_t n)
{
	unsigned char *p = s + n;
	while (p-- > s)
		if (*p == (unsigned char)c)
			return p;
	return NULL;
}
