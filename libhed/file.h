/* The file handling */

/* This is a libhed PUBLIC file.
 * This header file will be installed on the target system.
 * When you add new things here, make sure they start with hed_.
 */

/*
 * hed - Hexadecimal editor
 * Copyright (C) 2004  Petr Baudis <pasky@ucw.cz>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of version 2 of the GNU General Public License as
 * published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef LIBHED__FILE_H
#define LIBHED__FILE_H

#include <libhed/config.h>

#include <stdbool.h>
#include <string.h>

#include <libhed/types.h>
#include <libhed/expr.h>

/* The block cache is organized as follows:
 *
 * At the start, there are two virtual blocks:
 * --
 *
 * The first block covers the edited file (and has a physical size
 * equal to the size of the file), the second block covers all
 * possible file offsets beyond EOF (and has zero physical size).
 *
 * Then we can do a read and turn the virtual block to two, or shift its
 * start/end:
 * -x-
 *
 * After a while...
 * x-xxx-xx-x-xxxxxxx-x
 *
 * As you can see, virtual blocks can have any size and stretch variably
 * and fill the gaps between real blocks which have a size limit.
 *
 * Now, we can dirty some blocks:
 * x-xxX-xx-x-xxxxxxx-x
 *
 * Even if we insert anything to the block, other blocks aren't touched,
 * except when the block size hits the limit - then, the block will spawn
 * another one:
 * x-xxXX-xx-x-xxxxxxx-x
 *
 * Again, we don't touch other blocks during deletion. Only when the block
 * size hits zero, we kill it:
 * x-xxXX-xx-x-xxxxxx-x
 *
 * Note that we still keep around blocks which have any counterpart in the
 * file, even if they are already dead in our current file image. This is
 * necessary when reading more file blocks to the cache and when commiting
 * the changes, in order to maintain offset consistency.
 *
 * When can size != phys_size?
 * * byte(s) have been INSERTED into this block: size > phys_size
 *	Special case is an entire new block:
 *		in case of virtual blocks, phys_size MUST == 0 (gap)
 * * byte(s) have been ERASED from this block: size < phys_size
 *	Virtual blocks are not permitted
 */

struct hed_block {
	struct hed_tree_node t;
	struct hed_list_head lru;

	/* Flags - see HED_BLOCK_xxx below. */
	long flags;

	/* List of hed_cursor_t offsets which reference this
	 * structure.  This is more flexible than refcounting,
	 * because you may actually do something to the file_block,
	 * as long as you update all the hed_cursor_t structs.
	 */
	struct hed_list_head refs;

	/* These are only valid for non-virtual blocks */
	struct hed_block_data *dataobj;
	size_t dataoff;

	/* Physical position of this block. */
	hed_uoff_t phys_pos;
};

#define HED_BLOCK_DIRTY     0x01	/* data has been modified */
#define HED_BLOCK_INSERTED  0x02	/* insert/delete block */
#define HED_BLOCK_EXCACHE   0x04	/* block is NOT in the cache[] array
					   and can be freed after it is
					   undirtied */
#define HED_BLOCK_VIRTUAL   0x08	/* "Virtual" block - must never be
					   dirty nor excache; data is not
					   valid - only size is meaningful. */
#define HED_BLOCK_EOF       0x10	/* Block beyond physical EOF */
#define HED_BLOCK_BAD       0x20	/* Block with read errors */
#define HED_BLOCK_TERMINAL  0x40	/* Terminating block */
#define HED_BLOCK_EMPTY     0x80	/* Empty block */

#define hed_block_is_dirty(b)		((b)->flags & HED_BLOCK_DIRTY)
#define hed_block_set_dirty(b)		((b)->flags |= HED_BLOCK_DIRTY)
#define hed_block_clear_dirty(b)	((b)->flags &= ~HED_BLOCK_DIRTY)

#define hed_block_is_inserted(b)	((b)->flags & HED_BLOCK_INSERTED)
#define hed_block_set_inserted(b)	((b)->flags |= HED_BLOCK_INSERTED)
#define hed_block_clear_inserted(b)	((b)->flags &= ~HED_BLOCK_INSERTED)

#define hed_block_is_excache(b)		((b)->flags & HED_BLOCK_EXCACHE)
#define hed_block_set_excache(b)	((b)->flags |= HED_BLOCK_EXCACHE)
#define hed_block_clear_excache(b)	((b)->flags &= ~HED_BLOCK_EXCACHE)

#define hed_block_is_virtual(b)		((b)->flags & HED_BLOCK_VIRTUAL)
#define hed_block_set_virtual(b)	((b)->flags |= HED_BLOCK_VIRTUAL)
#define hed_block_clear_virtual(b)	((b)->flags &= ~HED_BLOCK_VIRTUAL)

#define hed_block_is_eof(b)		((b)->flags & HED_BLOCK_EOF)
#define hed_block_set_eof(b)		((b)->flags |= HED_BLOCK_EOF)
#define hed_block_clear_eof(b)		((b)->flags &= ~HED_BLOCK_EOF)

#define hed_block_is_bad(b)		((b)->flags & HED_BLOCK_BAD)
#define hed_block_set_bad(b)		((b)->flags |= HED_BLOCK_BAD)
#define hed_block_clear_bad(b)		((b)->flags &= ~HED_BLOCK_BAD)

#define hed_block_is_terminal(b)	((b)->flags & HED_BLOCK_TERMINAL)
#define hed_block_set_terminal(b)	((b)->flags |= HED_BLOCK_TERMINAL)
#define hed_block_clear_terminal(b)	((b)->flags &= ~HED_BLOCK_TERMINAL)

#define hed_block_is_empty(b)		((b)->flags & HED_BLOCK_EMPTY)
#define hed_block_set_empty(b)		((b)->flags |= HED_BLOCK_EMPTY)
#define hed_block_clear_empty(b)	((b)->flags &= ~HED_BLOCK_EMPTY)

/* flags related to block allocation */
#define HED_BLOCK_ALLOCMASK	(HED_BLOCK_EXCACHE | HED_BLOCK_TERMINAL)

/* flags related to block state */
#define HED_BLOCK_STATEMASK	(HED_BLOCK_DIRTY | HED_BLOCK_INSERTED | \
				 HED_BLOCK_VIRTUAL | HED_BLOCK_EOF | \
				 HED_BLOCK_BAD)

static inline hed_uoff_t
hed_block_size(const struct hed_block *block)
{
	return block->t.size;
}

static inline unsigned char *
hed_block_data(const struct hed_block *block)
{
	return block->dataobj
		? block->dataobj->data + block->dataoff
		: NULL;
}

/* Returns true iff the block follows a deletion */
bool hed_block_is_after_erase(struct hed_block *block);

/* Returns true iff the block follows an insertion */
bool hed_block_is_after_insert(struct hed_block *block);

/* A hed_cursor_t consists of file position, its corresponding
 * block pointer and the offset within that block.
 */
typedef struct {
	hed_uoff_t pos;
	struct hed_block *block;
	hed_uoff_t off;
	struct hed_list_head list;
} hed_cursor_t;

#define HED_NULL_CURSOR	{ 0, NULL, 0 }

#define hed_is_a_cursor(curs)	(!!(curs)->block)

/* Return a pointer to the actual data at @curs, or NULL if none */
static inline void *
hed_cursor_data(const hed_cursor_t *curs)
{
	return curs->block->dataobj
		? hed_block_data(curs->block) + curs->off
		: NULL;
}

/* Returns the physical file position of the cursor */
static inline hed_uoff_t
hed_cursor_phys_pos(const hed_cursor_t *curs)
{
	return hed_block_is_inserted(curs->block)
		? curs->block->phys_pos
		: curs->block->phys_pos + curs->off;
}

/* Returns the number of contiguous bytes following @curs. */
static inline hed_uoff_t
hed_cursor_span(const hed_cursor_t *curs)
{
	return hed_block_size(curs->block) - curs->off;
}

/* Returns the number of contiguous bytes following @curs,
 * but at most @maxlen.
 * The intended use of this function is to determine the next chunk
 * length to read @maxlen bytes from the file.
 */
static inline size_t
hed_cursor_chunk_len(const hed_cursor_t *curs, size_t maxlen)
{
	hed_uoff_t ret = hed_cursor_span(curs) - 1;
	return !hed_block_is_empty(curs->block)
		? ret < maxlen ? (size_t)ret + 1 : maxlen
		: 0;
}

/* File object */

enum hed_readahead {
	HED_RA_NONE,		/* No readahead */
	HED_RA_FORWARD,		/* Read-ahead forwards */
	HED_RA_BACKWARD		/* Read-ahead backwards */
};

struct hed_file {
	const char *name;	/* file name */

	hed_uoff_t phys_size;	/* physical size on disk */
	hed_uoff_t size;	/* size after modifications */

	bool modified;
};

/*******************************************************************
 * file object API
 */

/* Global initialization */
int libhed_init(void);

/* Opens a file. */
struct hed_file *hed_open(const char *name);

/* Closes the file. */
void hed_close(struct hed_file *file);

/* Get the current file size */
static inline
hed_uoff_t hed_file_size(struct hed_file *file)
{
	return file->size;
}

/* Returns true iff the file has been modified */
static inline bool
hed_file_is_modified(struct hed_file *file)
{
	return file->modified;
}

/* Set the readahead policy hint */
void hed_file_set_readahead(struct hed_file *file, enum hed_readahead val);

/* Update the file size from the file system */
int hed_file_update_size(struct hed_file *file);

/* Commits dirty blocks to the file. */
int hed_file_commit(struct hed_file *file);

/* Saves dump of internal structures to disk. */
int hed_file_write_swap(struct hed_file *file);

/* Loads dump of internal structures associated with given file from disk. */
int hed_file_read_swap(struct hed_file *file);

/* Checks whether a swap file is available. */
int hed_file_has_swap(struct hed_file *file);

/* Returns the swap file name. */
char *hed_file_swap_name(struct hed_file *file);

/* Removes the swap file from disk. */
int hed_file_remove_swap(struct hed_file *file);

/* Converts a logical @offset to a cursor (@curs). */
void hed_get_cursor(struct hed_file *file, hed_uoff_t offset,
		    hed_cursor_t *curs);

/* Update the position of an initialized cursor to @offset. */
void hed_update_cursor(struct hed_file *file, hed_uoff_t offset,
		       hed_cursor_t *curs);

/* Mark @curs as no longer needed. In particular, this stops tracking
 * file changes with this pointer.
 */
void hed_put_cursor(hed_cursor_t *curs);

/* Duplicate into an empty hed_cursor_t */
void hed_dup_cursor(const hed_cursor_t *src, hed_cursor_t *dst);

/* Duplicate into an initialized hed_cursor_t */
void hed_dup2_cursor(const hed_cursor_t *src, hed_cursor_t *dst);

/* Relative move from a given block offset.
 * If the resulting position is out of bounds (either < 0 or > OFF_MAX),
 * the offset is adjusted to only move to 0 or OFF_MAX.
 * Returns the actual offset which was used.
 */
hed_off_t hed_move_relative(hed_cursor_t *curs, hed_off_t num);

/* Ensure reads are valid from a block offset.
 * If @curs points to a virtual block (and not beyond EOF), the
 * corresponding physical block is loaded from the file.
 * Returns the number of bytes available in the next chunk
 * (cf. hed_cursor_chunk_len), or ZERO on error.
 */
size_t hed_prepare_read(struct hed_file *file, const hed_cursor_t *curs,
			size_t len);

/* Given you know that offset is just one byte beyond the last span,
 * get the block which contains byte at @curs.
 * If the block is not available, it is loaded from disk, so do not
 * use this function if you only want to iterate through virtual
 * blocks.
 * Returns non-zero on error, and the error code is available in @errno.
 */
int hed_file_next_block(struct hed_file *file, hed_cursor_t *curs);

/* Copy in @count bytes from file @file at @pos to @buf.
 * Returns number of bytes which were not copied, i.e. zero on success.
 */
size_t hed_file_cpin(struct hed_file *file, void *buf, size_t count,
		     const hed_cursor_t *pos);

/* Find a given sequence of bytes in the file, starting at the @pos file
 * offset and possibly wrapping around. @dir is +1 or -1. */
int hed_file_find_expr(struct hed_file *file, hed_cursor_t *pos, int dir,
		       struct hed_expr *expr);
#define HED_FINDOFF_NO_MATCH	-1
#define HED_FINDOFF_ERROR	-2

/* Sets a single byte in the file. */
int hed_file_set_byte(struct hed_file *file, hed_cursor_t *curs,
		      unsigned char byte);
/* Sets multiple bytes in the file. */
size_t hed_file_set_block(struct hed_file *file, hed_cursor_t *curs,
			  unsigned char *buf, size_t len);
hed_uoff_t hed_file_set_bytes(struct hed_file *file, hed_cursor_t *curs,
			      unsigned char byte, hed_uoff_t rep);

/* To insert bytes:
 *  1. call file_insert_begin() with the cursor position and store the
 *     insert physical position.
 *  2. call file_insert_byte() and/or file_insert_block() on the insert pos,
 *     possibly repeatedly
 *  3. call file_insert_end() on the insert pos and forget all about it;
 *     be careful with aliases, because the insert might get merged/freed
 */
int hed_file_insert_begin(struct hed_file *file,
			  const hed_cursor_t *curs, hed_cursor_t *curs_ins);
void hed_file_insert_end(struct hed_file *file, hed_cursor_t *curs_ins);

/* Inserts a single byte to the file. */
int hed_file_insert_byte(struct hed_file *file, hed_cursor_t *curs,
			 unsigned char byte);
/* Insert a block of bytes to the file. */
size_t hed_file_insert_block(struct hed_file *file, hed_cursor_t *curs,
			     unsigned char *buf, size_t len);

/* The following function is a shorthand for the usual
 * file_insert_begin, file_insert_block, file_insert_end
 * sequence.
 */
size_t hed_file_insert_once(struct hed_file *file, hed_cursor_t *curs,
			    unsigned char *buf, size_t len);

/* Erases a single byte from the file. */
# define hed_file_erase_byte(file,curs)	(hed_file_erase_block((file),(curs),1))

/* Erases a block of bytes from the file. */
size_t hed_file_erase_block(struct hed_file *file, hed_cursor_t *curs,
			    hed_uoff_t len);

#endif
