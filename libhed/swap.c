/*
 * hed - Hexadecimal editor
 * Copyright (C) 2011  Petr Tesarik <petr@tesarici.cz>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of version 2 of the GNU General Public License as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/* Feature macros needed for:
 *  - pread, pwrite
 */
#define _GNU_SOURCE

#include <config.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <assert.h>

#include <util/lists.h>
#include "swap.h"
#include "access.h"

#ifdef HED_CONFIG_SWAP

#undef SWAP_DEBUG
#ifdef SWAP_DEBUG
# include <stdio.h>
# define SDEBUG(x...) fprintf(stderr, x)
#else
# define SDEBUG(x...)
#endif

/* A swap file structure:
 *
 * 0					File header
 * SWAP_BLOCK_SIZE			1st mapping
 * ...					Allocated blocks
 * SWAP_BLOCK_SIZE + SWP_MAPEXTENT	2nd mapping
 * ...
 */

struct swp_block {
	struct list_head list;
	hed_uoff_t pos;
	void **paddr;
	struct list_head alloclist;
	struct list_head freelist;
};

struct swp_alloc {
	struct list_head list;
	unsigned off;		/* Offset from block beginning */
	unsigned size;		/* Size is max SWAP_BLOCK_SIZE */
};

#define SWP_MAPSIZE	((SWAP_BLOCK_SIZE) / sizeof(void*))
#define SWP_MAPMASK	((signed long)SWP_MAPSIZE - 1)
#define SWP_MAPEXTENT	((SWAP_BLOCK_SIZE) * (SWP_MAPSIZE))
#define SWP_MAPEXTSHIFT (2*SWAP_BLOCK_SHIFT - bitsize(sizeof(void*)))
#define SWP_ALLOC_ALIGN	(8)

static struct swp_block *swp_bappend(struct swp_file *swp);

#ifdef SWAP_DEBUG

static void
swp_dump(struct swp_file *swp)
{
	struct swp_block *block;
	struct swp_alloc *a;

	fputs("-- swap file dump --\n", stderr);

	fprintf(stderr, "size: %ld\n", (long) swp->size);

	fputs("free blocks:\n", stderr);
	list_for_each_entry(block, &swp->freeblocks, list) {
		fprintf(stderr, "%p: [%p] @freed %ld\n",
			block, block->paddr, (long) block->pos);
		assert(list_empty(&block->alloclist));
		assert(list_empty(&block->freelist));
	}

	fputs("used blocks:\n", stderr);
	list_for_each_entry(block, &swp->usedblocks, list) {
		fprintf(stderr, "%p: [%p] @%p %ld\n",
			block, block->paddr, *block->paddr,
			(long) block->pos);

		if (!list_empty(&block->alloclist))
			fputs("  allocations:\n", stderr);
		list_for_each_entry(a, &block->alloclist, list)
			fprintf(stderr, "  %p: 0x%x + %u\n",
				a, a->off, a->size);

		if (!list_empty(&block->freelist))
			fputs("  free list: \n", stderr);
		list_for_each_entry (a, &block->freelist, list)
			fprintf(stderr, "  %p: 0x%x + %u\n",
				a, a->off, a->size);
	}

	fputs("-- swap file dump end--\n", stderr);
}

#else  /* SWAP_DEBUG */

#define swp_dump(s)	do{} while(0)

#endif

#ifdef HED_CONFIG_SWAP_MMAP

#include <sys/mman.h>

#define SWP_PAGE_INVALID	MAP_FAILED

static inline void
swp_init_flags(struct swp_file *swp, int flags)
{
	swp->mmap_prot = (flags & O_ACCMODE) == O_RDWR
		? PROT_READ | PROT_WRITE
		: PROT_READ;
	swp->mmap_flags = swp->fd < 0
		? MAP_PRIVATE | MAP_ANONYMOUS
		: MAP_SHARED;
}

/* Set the file size. */
static int
swp_setsize(struct swp_file *swp)
{
	return swp->fd >= 0
		? ftruncate(swp->fd, swp->size + SWAP_BLOCK_SIZE)
		: 0;
}

/* Allocate data for a file block */
static void *
swp_page_alloc(struct swp_file *swp, hed_uoff_t pos)
{
	return mmap(NULL, SWAP_BLOCK_SIZE,
		    swp->mmap_prot, swp->mmap_flags,
		    swp->fd, pos);
}

/* De-allocate data for a file block */
static inline void
swp_page_free(struct swp_file *swp, void *ptr)
{
	munmap(ptr, SWAP_BLOCK_SIZE);
}

static inline size_t
swp_page_read(struct swp_file *swp, void *ptr, hed_uoff_t pos)
{
	return 0;
}

static inline size_t
swp_page_write(struct swp_file *swp, void *ptr, hed_uoff_t pos)
{
	return msync(ptr, SWAP_BLOCK_SIZE, MS_ASYNC);
}

#else  /* HED_CONFIG_SWAP_MMAP */

#define SWP_PAGE_INVALID	NULL

static inline void
swp_init_flags(struct swp_file *swp, int flags)
{
}

/* Set the file size. */
static inline int
swp_setsize(struct swp_file *swp)
{
	return 0;
}

/* Allocate data for a file block */
static inline void *
swp_page_alloc(struct swp_file *swp, hed_uoff_t pos)
{
	return malloc(SWAP_BLOCK_SIZE);
}

/* De-allocate data for a file block */
static inline void
swp_page_free(struct swp_file *swp, void *ptr)
{
	free(ptr);
}

static inline size_t
swp_page_read(struct swp_file *swp, void *ptr, hed_uoff_t pos)
{
	return SWAP_BLOCK_SIZE - pread(swp->fd, ptr, SWAP_BLOCK_SIZE, pos);
}

static inline size_t
swp_page_write(struct swp_file *swp, void *ptr, hed_uoff_t pos)
{
	return SWAP_BLOCK_SIZE - pwrite(swp->fd, ptr, SWAP_BLOCK_SIZE, pos);
}

#endif	/* HED_CONFIG_SWAP_MMAP */

static inline int
swp_close(struct swp_file *swp)
{
	int ret = swp->fd >= 0
		? close(swp->fd)
		: 0;
	swp->fd = -1;
	return ret;
}

static struct swp_file *
swp_init(const char *name, int openflags)
{
	struct swp_file *swp = calloc(1, sizeof(struct swp_file));
	if (swp) {
		INIT_LIST_HEAD(&swp->freeblocks);
		INIT_LIST_HEAD(&swp->usedblocks);
		swp->fd = open(name, openflags, 0664);
		swp_init_flags(swp, openflags);
	}
	return swp;
}

struct swp_file *
swp_init_write(const char *name)
{
	struct swp_file *swp;
	struct swp_header *hdr;

	if (! (swp = swp_init(name, O_RDWR | O_CREAT)) )
		return NULL;

	if (swp_setsize(swp))
		goto fail;
	hdr = swp_page_alloc(swp, 0);
	if (hdr == SWP_PAGE_INVALID)
		goto fail;
	swp->hdr = hdr;

	memcpy(hdr->signature, SWAP_SIGNATURE, SIG_LEN);
	hdr->version = SWAP_VERSION;
	hdr->offsize = sizeof(hed_off_t);
	hdr->addr = hdr;

	return swp;

 fail:
	swp_done(swp);
	return NULL;
}

int
swp_done(struct swp_file *swp)
{
	struct swp_alloc *cura, *nexta;
	struct swp_block *curb, *nextb;
	int ret;

	/* Free all swap file pages */
	list_for_each_entry_safe_reverse(curb, nextb, &swp->usedblocks, list) {
		list_for_each_entry_safe(cura, nexta, &curb->alloclist, list)
			free(cura);
		list_for_each_entry_safe(cura, nexta, &curb->freelist, list)
			free(cura);
		swp_page_free(swp, *curb->paddr);
		free(curb);
	}
	list_for_each_entry_safe(curb, nextb, &swp->freeblocks, list)
		free(curb);
	if (swp->maps)
		free(swp->maps);
	if (swp->hdr)
		swp_page_free(swp, swp->hdr);

	ret = swp_close(swp);
	free(swp);
	return ret;
}

/* Get an existing mapping */
static void **
swp_getmapping(struct swp_file *swp, hed_uoff_t pos)
{
	unsigned mapnum = pos >> SWP_MAPEXTSHIFT;
	unsigned long mapoff = (pos >> SWAP_BLOCK_SHIFT) & SWP_MAPMASK;
	assert(mapnum < swp->nmaps);
	return swp->maps[mapnum] + mapoff;
}

/* Append a new mapping at EOF */
static void **
swp_appendmapping(struct swp_file *swp)
{
	if (swp->size == (swp->nmaps << SWP_MAPEXTSHIFT)) {
		void ***mapvec;
		void **newmap;
		struct swp_block *block;

		mapvec = realloc(swp->maps,
				 (swp->nmaps + 1) * sizeof(void **));
		if (!mapvec)
			return NULL;
		swp->maps = mapvec;

		newmap = swp_page_alloc(swp, swp->size + SWAP_BLOCK_SIZE);
		if (newmap == SWP_PAGE_INVALID)
			return NULL;
		swp->maps[swp->nmaps++] = newmap;

		block = swp_bappend(swp);
		if (!block) {
			--swp->nmaps;
			swp_page_free(swp, newmap);
			return NULL;
		}
		*block->paddr = newmap;
	}
	return swp_getmapping(swp, swp->size);
}

/* Mark a mapping as free */
static void
swp_freemapping(struct swp_file *swp, void **paddr)
{
	*paddr = SWP_PAGE_INVALID;

	if (swp->size == ((swp->nmaps-1) << SWP_MAPEXTSHIFT) + SWAP_BLOCK_SIZE) {
		struct swp_block *block;

		assert(!list_empty(&swp->usedblocks));
		block = list_entry(swp->usedblocks.prev,
				   struct swp_block, list);

		assert(*block->paddr == swp->maps[swp->nmaps-1]);
		assert(block->paddr == *block->paddr);

		list_del(&block->list);
		swp_page_free(swp, block->paddr);
		swp->size -= SWAP_BLOCK_SIZE;
		swp_setsize(swp);
		--swp->nmaps;
	}
}

/* Append a page at EOF */
static struct swp_block *
swp_bappend(struct swp_file *swp)
{
	struct swp_block *ret;
	if (! (ret = malloc(sizeof(struct swp_block))) )
		goto err;
	if (! (ret->paddr = swp_appendmapping(swp)) )
		goto err_free;

	INIT_LIST_HEAD(&ret->list);
	ret->pos = swp->size;
	INIT_LIST_HEAD(&ret->alloclist);
	INIT_LIST_HEAD(&ret->freelist);

	swp->size += SWAP_BLOCK_SIZE;
	if (swp_setsize(swp))
		goto err_free_map;
	list_add_tail(&ret->list, &swp->usedblocks);
	return ret;

 err_free_map:
	swp_freemapping(swp, ret->paddr);
 err_free:
	free(ret);
 err:
	return NULL;
}

static void
swp_bfree(struct swp_file *swp, struct swp_block *block)
{
	struct swp_block *cur;

	if (*block->paddr != SWP_PAGE_INVALID)
		swp_page_free(swp, *block->paddr);

	list_for_each_entry_reverse(cur, &swp->freeblocks, list) {
		if (cur->pos < block->pos)
			break;
	}
	list_move(&block->list, &cur->list);

	while (swp->freeblocks.prev != &swp->freeblocks) {
		struct swp_block *cur = list_entry(swp->freeblocks.prev,
						   struct swp_block, list);
		if (cur->pos != swp->size - SWAP_BLOCK_SIZE)
			break;

		swp->size -= SWAP_BLOCK_SIZE;
		swp_setsize(swp);
		swp_freemapping(swp, cur->paddr);
		list_del(&cur->list);
		free(cur);
	}
}

static struct swp_block *
swp_balloc(struct swp_file *swp)
{
	struct swp_block *ret = list_entry(swp->freeblocks.next,
					   struct swp_block, list);

	if (&ret->list != &swp->freeblocks) {
		struct swp_block *cur;
		list_for_each_entry_reverse(cur, &swp->usedblocks, list)
			if (cur->pos < ret->pos)
				break;
		list_move(&ret->list, &cur->list);
	} else if (! (ret = swp_bappend(swp)) )
		return NULL;

	*ret->paddr = swp_page_alloc(swp, ret->pos + SWAP_BLOCK_SIZE);
	if (*ret->paddr == SWP_PAGE_INVALID) {
		swp_bfree(swp, ret);
		return NULL;
	}

	return ret;
}

static struct swp_alloc *
swp_malloc_new(struct swp_file *swp, struct swp_block **pblock)
{
	struct swp_block *block;
	struct swp_alloc *ret;

	if (! (ret = malloc(sizeof(struct swp_alloc))) )
		goto fail;
	if (! (block = swp_balloc(swp)) )
		goto fail_free;

	ret->off = 0;
	ret->size = SWAP_BLOCK_SIZE;

	*pblock = block;
	return ret;

 fail_free:
	free(ret);
 fail:
	return NULL;
}

static struct list_head *
freelist_pos(struct swp_block *block, unsigned off)
{
	struct swp_alloc *a;
	list_for_each_entry(a, &block->freelist, list)
		if (a->off >= off)
			break;
	return &a->list;
}

static void
swp_split(struct swp_block *block, struct swp_alloc *a, unsigned splitpoint)
{
	unsigned remain;
	struct list_head *pos;
	struct swp_alloc *next, *prev;

	splitpoint += SWP_ALLOC_ALIGN - 1;
	splitpoint -= splitpoint % SWP_ALLOC_ALIGN;
	remain = a->size - splitpoint;

	if (!remain)
		return;

	/* Find the neighbours */
	pos = freelist_pos(block, a->off);
	if (pos != &block->freelist) {
		next = list_entry(pos, struct swp_alloc, list);
		if (next->off != a->off + a->size)
			next = NULL;
	} else
		next = NULL;

	pos = pos->prev;
	if (pos != &block->freelist) {
		prev = list_entry(pos, struct swp_alloc, list);
		if (prev->off + prev->size != a->off)
			prev = NULL;
	} else
		prev = NULL;

	if (prev && next) {
		if (!splitpoint) {
			prev->size += next->size;
			list_del(&next->list);
			free(next);
		} else if (prev->size < next->size)
			prev = NULL;
	}

	if (prev) {
		prev->size += remain;
	} else if (next) {
		next->off -= remain;
		next->size += remain;
	} else if (splitpoint) {
		if (! (next = malloc(sizeof(struct swp_alloc))) )
			return;
		next->off = a->off + splitpoint;
		next->size = remain;
		list_add(&next->list, pos);
	} else {
		list_move(&a->list, pos);
		return;
	}

	a->size = splitpoint;
	if (!splitpoint) {
		list_del(&a->list);
		free(a);
	}
}

static struct swp_alloc *
find_free_alloc(struct swp_file *swp, size_t size, struct swp_block **pblock,
		struct list_head *endnode)
{
	struct list_head *blknode;

	assert(size <= SWAP_BLOCK_SIZE);

	blknode = swp->usedblocks.next;
	while (blknode != endnode) {
		struct swp_block *block =
			list_entry(blknode, struct swp_block, list);
		struct swp_alloc *a;
		list_for_each_entry(a, &block->freelist, list) {
			if (a->size >= size) {
				list_del(&a->list);
				*pblock = block;
				return a;
			}
		}
		blknode = blknode->next;
	}

	return NULL;
}

void *
swp_malloc(struct swp_file *swp, size_t size)
{
	struct swp_block *block;
	struct swp_alloc *a;

	a = find_free_alloc(swp, size, &block, &swp->usedblocks);
	if (!a && ! (a = swp_malloc_new(swp, &block)) )
		return NULL;

	list_add(&a->list, &block->alloclist);

	swp_split(block, a, size);

	SDEBUG("Allocated %ld bytes at %p\n",
	       (long)size, *block->paddr + a->off);
	swp_dump(swp);

	return *block->paddr + a->off;
}

void *
swp_zalloc(struct swp_file *swp, size_t size)
{
	void *ret = swp_malloc(swp, size);
	if (ret)
		memset(ret, 0, size);
	return ret;
}

static void
do_free(struct swp_file *swp, struct swp_block *block, struct swp_alloc *a)
{
	swp_split(block, a, 0);

	if (list_empty(&block->alloclist)) {
		struct swp_alloc *cur, *next;
		list_for_each_entry_safe(cur, next, &block->freelist, list)
			free(cur);
		INIT_LIST_HEAD(&block->freelist);
		swp_bfree(swp, block);
	}
}

static struct swp_alloc *
swp_find_alloc(struct swp_file *swp, void *ptr, struct swp_block **pblock)
{
	struct swp_block *block;
	struct swp_alloc *a;
	unsigned off = 0;

	list_for_each_entry(block, &swp->usedblocks, list) {
		off = ptr - *block->paddr;
		if (off < SWAP_BLOCK_SIZE)
			break;
	}

	/* Fail if we cannot find the block */
	assert(&block->list != &swp->usedblocks);

	list_for_each_entry(a, &block->alloclist, list)
		if (a->off == off)
			break;

	/* Fail if we get an unallocated @ptr */
	assert(&a->list != &block->alloclist);

	*pblock = block;
	return a;
}

void *
swp_shrink(struct swp_file *swp, void *ptr, size_t newsize)
{
	struct swp_block *block, *newblock;
	struct swp_alloc *a, *newa;

	SDEBUG("Shrink %p to %ld\n", ptr, (long)newsize);

	a = swp_find_alloc(swp, ptr, &block);

	newa = find_free_alloc(swp, newsize, &newblock, &block->list);
	if (newa) {
		list_add(&newa->list, &newblock->alloclist);
		memcpy(*newblock->paddr + newa->off, ptr, newsize);
		do_free(swp, block, a);
		a = newa;
		block = newblock;
	}
	swp_split(block, a, newsize);

	swp_dump(swp);
	return *block->paddr + a->off;
}

void
swp_free(struct swp_file *swp, void *ptr)
{
	struct swp_alloc *a;
	struct swp_block *block;

	SDEBUG("Free %p\n", ptr);

	a = swp_find_alloc(swp, ptr, &block);
	do_free(swp, block, a);

	swp_dump(swp);
}

int
swp_write(struct swp_file *swp)
{
	struct swp_block *block;
	int ret = 0;

	SDEBUG("Write swap file\n");
	swp_dump(swp);

	if (swp_page_write(swp, swp->hdr, 0))
		ret = -1;

	list_for_each_entry(block, &swp->usedblocks, list)
		if (swp_page_write(swp, *block->paddr,
				   block->pos + SWAP_BLOCK_SIZE))
			ret = -1;

	return ret;
}

static int
read_header(struct swp_file *swp)
{
	struct swp_header *hdr;
	unsigned i;
	hed_off_t size;
	hed_off_t mapoff;

	/* Read the header block */
	hdr = swp_page_alloc(swp, 0);
	if (hdr == SWP_PAGE_INVALID)
		return -1;
	swp->hdr = hdr;

	if (swp_page_read(swp, hdr, 0))
		return -1;

	/* Sanity checks */
	if (memcmp(hdr->signature, SWAP_SIGNATURE, SIG_LEN))
		return -1;
	if (hdr->version != SWAP_VERSION)
		return -1;
	if (hdr->offsize != sizeof(hed_off_t))
		return -1;

	/* Allocate the maps */
	if ( (size = lseek(swp->fd, 0, SEEK_END)) < 0)
		return -1;
	swp->size = size - SWAP_BLOCK_SIZE;
	swp->nmaps = ((swp->size - SWAP_BLOCK_SIZE) >> SWP_MAPEXTSHIFT) + 1;
	swp->maps = calloc(swp->nmaps, sizeof(void **));
	if (!swp->maps)
		return -1;

	/* Read in the maps */
	mapoff = SWAP_BLOCK_SIZE;
	for (i = 0; i < swp->nmaps; ++i, mapoff += SWP_MAPEXTENT) {
		swp->maps[i] = swp_page_alloc(swp, mapoff);
		if (swp->maps[i] == SWP_PAGE_INVALID)
			return -1;
		if (swp_page_read(swp, swp->maps[i], mapoff))
			return -1;
	}

	return 0;
}

struct swp_file *
swp_init_read(const char *name)
{
	struct swp_file *swp;

	if (! (swp = swp_init(name, O_RDONLY)) )
		return NULL;

	if (read_header(swp)) {
		swp_done(swp);
		return NULL;
	}

	return swp;
}

static hed_off_t
addr2pos(struct swp_file *swp, void *ptr)
{
	unsigned i, j;
	for (i = 0; i < swp->nmaps; ++i)
		for (j = 0; j < SWP_MAPSIZE; ++j) {
			size_t off = ptr - swp->maps[i][j];
			if (off < SWAP_BLOCK_SIZE)
				return SWAP_BLOCK_SIZE + off +
					(i << SWP_MAPEXTSHIFT) +
					(j << SWAP_BLOCK_SHIFT);
		}
	return 0;
}

size_t
swp_cpin(struct swp_file *swp, void *buf, void *ptr, size_t len)
{
	size_t off;
	hed_off_t pos;

	off = ptr - swp->hdr->addr;
	if (off < SWAP_BLOCK_SIZE)
		pos = off;
	else if (! (pos = addr2pos(swp, ptr)) ) {
		SDEBUG("Swap address %p not found!\n", ptr);
		return len;
	}
	SDEBUG("Translate swap address %p -> 0x%llx\n", ptr, pos);
	return len - pread(swp->fd, buf, len, pos);
}

#endif	/* HED_CONFIG_SWAP */
