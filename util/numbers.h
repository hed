/* $Id$ */

#ifndef HED__UTIL_NUMBERS_H
#define HED__UTIL_NUMBERS_H

/*
 * Bilbo was going to be eleventy-one, 111, a rather curious number and a very
 * respectable age for a hobbit (the Old Took himself had only reached 130);
 * and Frodo was going to be thirty-three, 33, an important number: the date
 * of his 'coming of age'.
 */

/* Maximal values of off_t and uoff_t.
 * uoff_t is unsigned, while off_t is signed; in 2's complement
 * arithmetic, this means that UOFF_MAX is all 1's, and OFF_MAX
 * has only the highest bit clear.
 */
#define UOFF_MAX (~(hed_uoff_t)(0))
#define OFF_MAX (hed_off_t)(UOFF_MAX >> 1)

/* The biggest possible integer type. */
typedef BINT_TYPE bint;
typedef unsigned BINT_TYPE ubint;

/* Minimax. */
static bint min(const bint a, const bint b);
static bint max(const bint a, const bint b);

/* Find the highest bit. */
static int bitsize(ubint x);



/**** Inline functions and such. */

static inline bint
min(const bint a, const bint b)
{
	return a < b ? a : b;
}

static inline bint
max(const bint a, const bint b)
{
	return a > b ? a : b;
}

static inline int
bitsize(ubint x)
{
	int r = 0;
	/* FIXME: only up to 64bits */
	if (x & 0xffffffff00000000ULL) { x >>= 32; r += 32; }
	if (x & 0x00000000ffff0000ULL) { x >>= 16; r += 16; }
	if (x & 0x000000000000ff00ULL) { x >>=  8; r +=  8; }
	if (x & 0x00000000000000f0ULL) { x >>=  4; r +=  4; }
	if (x & 0x000000000000000cULL) { x >>=  2; r +=  2; }
	if (x & 0x0000000000000002ULL) {           r +=  1; }
	return r;
}

/**** Endianity. */

/* Returns 1 if running on a little-endian architecture.
 * Hopefully, this will be always optimized to a constant expression.
 */
static inline int
arch_little_endian(void)
{
	union {
		long x;
		char c;
	} u = { 1 };
	return u.c;
}

#endif
