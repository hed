#### CONFIGURATION

PREFIX=/usr/local
BINDIR=$(PREFIX)/bin
INCDIR=$(PREFIX)/include
LIBDIR=$(PREFIX)/lib
MANDIR=$(PREFIX)/share/man

# gcc-4.1 needs -Wno-pointer-sign
CUSTOM_CFLAGS=-Wall -ggdb -O3
SYS_CFLAGS=
LIBS += -lncurses -ltinfo

LD=ld
AR=ar

### CONFIGURATION END

ifndef INSTALL
INSTALL=/usr/bin/install
endif

export


SUBDIRS=libhed src

all: all-recursive

install: install-recursive
	$(INSTALL) -D -m 644 doc/hed.1 $(DESTDIR)$(MANDIR)/man1/hed.1

install-recursive: all

clean: clean-recursive

-include Makefile.lib
